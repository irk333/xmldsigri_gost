package org.admnkz.gostxml.dsig.internal;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.crypto.KeySelector;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.token.X509Security;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Base64;
import org.apache.xpath.XPathAPI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/*
import org.apache.axis.Message;
import org.apache.axis.message.SOAPEnvelope;
*/
public class TestDsig {

	private Object[] samData = null;
	private SecureRandom random = null;
	private Provider xmlDSigProvider = null;
	private char[] storepwd = {'p','a','s','s','w','o','r','d'};
	
	@Before
	public void setUp() throws Exception 
	{
		org.apache.xml.security.Init.init();
		setSAMdata(new File("/home/irina/v/workspace/xmldsigri_gost/data/alice2/store.pfx"), storepwd, "Test key", storepwd);
		random = SecureRandom.getInstance("GOST28147PRNG", "SC");			
		random.setSeed("/home/irina/v/workspace/xmldsigri_gost/data/alice2/".getBytes());		
		
		xmlDSigProvider = new org.admnkz.gostxml.dsig.internal.dom.XMLDSigRI();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSOAP() throws IOException {
		String ssigned= getMessage();
//		String ssigned= FileUtils.readFileToString(new File("/home/irina/v/workspace/cryptoservice/data/vermsg.xml"),"UTF8");
//		Document adoc = signDoc(ssigned);
		boolean bres = verifyDoc(ssigned, false);
		Assert.assertTrue(bres);
	}
	
	@Test
	public void testVerifyFNS1() throws Exception 
	{
//		com.sun.org.apache.xml.internal.security.Init.init();
//		String sbase64 = "PFM6RW52ZWxvcGUgeG1sbnM6Uz0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOndzc2U9Imh0dHA6Ly9kb2NzLm9hc2lzLW9wZW4ub3JnL3dzcy8yMDA0LzAxL29hc2lzLTIwMDQwMS13c3Mtd3NzZWN1cml0eS1zZWNleHQtMS4wLnhzZCIgeG1sbnM6d3N1PSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIj48UzpIZWFkZXI+PHdzc2U6U2VjdXJpdHkgUzphY3Rvcj0iaHR0cDovL3NtZXYuZ29zdXNsdWdpLnJ1L2FjdG9ycy9zbWV2Ij48ZHM6U2lnbmF0dXJlIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj4KPGRzOlNpZ25lZEluZm8+CjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+CjxkczpTaWduYXR1cmVNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNnb3N0cjM0MTAyMDAxLWdvc3RyMzQxMSIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI2JvZHkiPgo8ZHM6VHJhbnNmb3Jtcz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIi8+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjZ29zdHIzNDExIi8+CjxkczpEaWdlc3RWYWx1ZT4zaUV6bEZVN1ZmblIrMXFLNUdBS0p1N3pWYzRHekJHVUd3enBibTJlYXZRPTwvZHM6RGlnZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVWYWx1ZT4KYUxqWVhUNU5Sc01qRml5RDVkQTlMMVdLS24wWU8xdVNVVVhvZ0hya2s4eUplQWhER2tnQWhuQSszdWpPRzdvMmhyVzhlbjJ0RkJXegpDcXNITmxER2h3PT0KPC9kczpTaWduYXR1cmVWYWx1ZT4KPGRzOktleUluZm8+Cjx3c3NlOlNlY3VyaXR5VG9rZW5SZWZlcmVuY2U+PHdzc2U6UmVmZXJlbmNlIFVSST0iI0ZOU0NlcnRpZmljYXRlIi8+PC93c3NlOlNlY3VyaXR5VG9rZW5SZWZlcmVuY2U+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+PHdzc2U6QmluYXJ5U2VjdXJpdHlUb2tlbiBFbmNvZGluZ1R5cGU9Imh0dHA6Ly9kb2NzLm9hc2lzLW9wZW4ub3JnL3dzcy8yMDA0LzAxL29hc2lzLTIwMDQwMS13c3Mtc29hcC1tZXNzYWdlLXNlY3VyaXR5LTEuMCNCYXNlNjRCaW5hcnkiIFZhbHVlVHlwZT0iaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3NzLzIwMDQvMDEvb2FzaXMtMjAwNDAxLXdzcy14NTA5LXRva2VuLXByb2ZpbGUtMS4wI1g1MDl2MyIgd3N1OklkPSJGTlNDZXJ0aWZpY2F0ZSI+TUlJRzB6Q0NCb0tnQXdJQkFnSUtHd2VBSmdBQUFBQUJTakFJQmdZcWhRTUNBZ013Z2dFeE1SZ3dGZ1lGS29VRFpBRVREVEV5TXpRMU5qYzRPVEF4TWpNeEdEQVdCZ2dxaFFNRGdRTUJBUk1LTVRJek5EVTJOemc1TURFb01DWUdBMVVFQ1F3ZjBLSFJnOUdKMExYUXN0R0IwTHJRdU5DNUlOQ3kwTERRdXlEUXRDNHlOakVYTUJVR0NTcUdTSWIzRFFFSkFSWUlZMkZBY25RdWNuVXhDekFKQmdOVkJBWVRBbEpWTVJVd0V3WURWUVFJREF6UW5OQyswWUhRdXRDeTBMQXhGVEFUQmdOVkJBY01ETkNjMEw3UmdkQzYwTExRc0RFa01DSUdBMVVFQ2d3YjBKN1FrTkNlSU5DZzBMN1JnZEdDMExYUXU5QzEwTHJRdnRDOE1UQXdMZ1lEVlFRTERDZlFvOUMwMEw3UmdkR0MwTDdRc3RDMTBZRFJqOUdPMFluUXVOQzVJTkdHMExYUXZkR0MwWUF4SlRBakJnTlZCQU1NSE5DaTBMWFJnZEdDMEw3UXN0R0wwTGtnMEtQUXBpRFFvTkNpMEpvd0hoY05NVEl3TnpBek1UTXdNakF3V2hjTk1UTXdOekF6TVRNeE1UQXdXakNCb1RFTE1Ba0dBMVVFQmhNQ1VsVXhhekJwQmdOVkJBb2VZZ1FrQkRVRU5BUTFCRUFFTUFRN0JFd0VQUVF3QkU4QUlBUWRCREFFT3dRK0JETUVQZ1F5QkRBRVR3QWdCQ0VFT3dSREJEWUVNUVF3QUNBRUlBUStCRUVFUVFRNEJEa0VRUVE2QkQ0RU9RQWdCQ1FFTlFRMEJEVUVRQVF3QkVZRU9BUTRNU1V3SXdZRFZRUURIaHdFSVFRY0JCSUFJQVFrQkIwRUlRQWdCQ0FFUGdSQkJFRUVPQVE0TUdNd0hBWUdLb1VEQWdJVE1CSUdCeXFGQXdJQ0pBQUdCeXFGQXdJQ0hnRURRd0FFUUlBRzUzR3IvY1hhMWtmTFprYUNSN0ExdTZWZ1dEcXQzNkdwNmZxUi8xVHpoTHd1aW5Gakp2MldhVHdLcVN1NTJHczhHSi9aa3RJL0FBUHFXUy8wOGhxamdnUUZNSUlFQVRBT0JnTlZIUThCQWY4RUJBTUNCUEF3SkFZRFZSMGxCQjB3R3dZR0tvVURaQUlDQmdjcWhRTUNBaUlHQmdnckJnRUZCUWNEQWpBZEJnTlZIUTRFRmdRVUcrWXNRZGtJOEdKTDB3eEV5OHU1S2JJTmVpVXdnZ0Z5QmdOVkhTTUVnZ0ZwTUlJQlpZQVVHcXpzcUpxeU1FdG5adVdyTENYcXF3cUJ3ZE9oZ2dFNXBJSUJOVENDQVRFeEdEQVdCZ1VxaFFOa0FSTU5NVEl6TkRVMk56ZzVNREV5TXpFWU1CWUdDQ3FGQXdPQkF3RUJFd294TWpNME5UWTNPRGt3TVNnd0pnWURWUVFKREIvUW9kR0QwWW5RdGRDeTBZSFF1dEM0MExrZzBMTFFzTkM3SU5DMExqSTJNUmN3RlFZSktvWklodmNOQVFrQkZnaGpZVUJ5ZEM1eWRURUxNQWtHQTFVRUJoTUNVbFV4RlRBVEJnTlZCQWdNRE5DYzBMN1JnZEM2MExMUXNERVZNQk1HQTFVRUJ3d00wSnpRdnRHQjBMclFzdEN3TVNRd0lnWURWUVFLREJ2UW50Q1EwSjRnMEtEUXZ0R0IwWUxRdGRDNzBMWFF1dEMrMEx3eE1EQXVCZ05WQkFzTUo5Q2owTFRRdnRHQjBZTFF2dEN5MExYUmdOR1AwWTdSaWRDNDBMa2cwWWJRdGRDOTBZTFJnREVsTUNNR0ExVUVBd3djMEtMUXRkR0IwWUxRdnRDeTBZdlF1U0RRbzlDbUlOQ2cwS0xRbW9JUVlreEhiemdVY3FaR3YzRExEUjQwR3pCWkJnTlZIUjhFVWpCUU1FNmdUS0JLaGtob2RIUndPaTh2TVRnNExqSTFOQzR4Tmk0NE9TOXlZUzlqWkhBdk1XRmhZMlZqWVRnNVlXSXlNekEwWWpZM05qWmxOV0ZpTW1NeU5XVmhZV0l3WVRneFl6RmtNeTVqY213d1J3WUlLd1lCQlFVSEFRRUVPekE1TURjR0NDc0dBUVVGQnpBQ2hpdG9kSFJ3T2k4dk1UZzRMakkxTkM0eE5pNDRPUzl5WVM5alpIQXZkR1Z6ZEY5allWOXlkR3N1WTNKME1FOEdCU3FGQTJSdkJFWU1SQ0xRbXRHQTBMalF2OUdDMEw3UW45R0EwTDRnUTFOUUlpQW8wTExRdGRHQTBZSFF1TkdQSURNdU5pa2dLTkM0MFlIUXY5QyswTHZRdmRDMTBMM1F1TkMxSURFcE1Dc0dBMVVkRUFRa01DS0FEekl3TVRJd056QXpNVE13TWpBd1dvRVBNakF4TXpBM01ETXhNekF5TURCYU1Db0dBMVVkSUFRak1DRXdDQVlHS29VRFpBSUNNQWtHQnlxRkF3SUNJZ1l3Q2dZSUt3WUJCUVVIQXdJd2dlVUdCU3FGQTJSd0JJSGJNSUhZRENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwREZRaTBLUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJQ0xRbXRHQTBMalF2OUdDMEw3UW45R0EwTDRnMEtQUXBpSWlJTkN5MExYUmdOR0IwTGpSanlBeExqVU1MTkNoMEtRdk1USTBMVEUxTkRNZzBMN1JnaUEwSU5DKzBMclJndEdQMExIUmdOR1BJREl3TVRBZzBMTXVEQ1hRb2RDa0x6RXlPQzB4TmpVNElOQyswWUlnTURFZzBMelFzTkdQSURJd01URWcwTE11TUFnR0JpcUZBd0lDQXdOQkFMR0xoR0VBUE8yZTlGN2JwUEs4eGZaSTRMOWxqeU9HVGMwb1I2MmcrUmgwV1pENCszLy9LTExMU2ExTjFWZTd5eTBmbG1MQVh4UUxXMUpUbHpmdThQST08L3dzc2U6QmluYXJ5U2VjdXJpdHlUb2tlbj48L3dzc2U6U2VjdXJpdHk+PC9TOkhlYWRlcj48UzpCb2R5IHdzdTpJZD0iYm9keSI+PHdzOlNlbmRSZXF1ZXN0UnMgeG1sbnM6d3M9Imh0dHA6Ly93cy51bmlzb2Z0LyIgeG1sbnM6Uz0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iPgo8c21ldjpNZXNzYWdlIHhtbG5zOnNtZXY9Imh0dHA6Ly9zbWV2Lmdvc3VzbHVnaS5ydS9yZXYxMTExMTEiPjxzbWV2OlNlbmRlcj48c21ldjpDb2RlPkZOUzAwMTAwMTwvc21ldjpDb2RlPjxzbWV2Ok5hbWU+0KTQndChINCg0L7RgdGB0LjQuDwvc21ldjpOYW1lPjwvc21ldjpTZW5kZXI+PHNtZXY6UmVjaXBpZW50PjxzbWV2OkNvZGU+MTMyNDI8L3NtZXY6Q29kZT48c21ldjpOYW1lPk1JTlNWWUFaX1NZU18xPC9zbWV2Ok5hbWU+PC9zbWV2OlJlY2lwaWVudD48c21ldjpPcmlnaW5hdG9yPjxzbWV2OkNvZGU+MTMyNDI8L3NtZXY6Q29kZT48c21ldjpOYW1lPk1JTlNWWUFaX1NZU18xPC9zbWV2Ok5hbWU+PC9zbWV2Ok9yaWdpbmF0b3I+PHNtZXY6VHlwZUNvZGU+R1NSVjwvc21ldjpUeXBlQ29kZT48c21ldjpTdGF0dXM+QUNDRVBUPC9zbWV2OlN0YXR1cz48c21ldjpEYXRlPjIwMTItMTAtMjVUMTA6NDg6NDNaPC9zbWV2OkRhdGU+PHNtZXY6RXhjaGFuZ2VUeXBlPjI8L3NtZXY6RXhjaGFuZ2VUeXBlPjxzbWV2OlNlcnZpY2VDb2RlPlNJRDAwMDM0NTE8L3NtZXY6U2VydmljZUNvZGU+PHNtZXY6Q2FzZU51bWJlcj44OTE2PC9zbWV2OkNhc2VOdW1iZXI+PHNtZXY6VGVzdE1zZy8+PC9zbWV2Ok1lc3NhZ2U+PHNtZXY6TWVzc2FnZURhdGEgeG1sbnM6c21ldj0iaHR0cDovL3NtZXYuZ29zdXNsdWdpLnJ1L3JldjExMTExMSI+PHNtZXY6QXBwRGF0YSB3c3U6SWQ9ImZucy1BcHBEYXRhIiB4bWxuczp3c3U9Imh0dHA6Ly9kb2NzLm9hc2lzLW9wZW4ub3JnL3dzcy8yMDA0LzAxL29hc2lzLTIwMDQwMS13c3Mtd3NzZWN1cml0eS11dGlsaXR5LTEuMC54c2QiPgo80JTQvtC60YPQvNC10L3RgiB4bWxucz0iaHR0cDovL3dzLnVuaXNvZnQvRk5TWkRML1JzMSIg0JLQtdGA0YHQpNC+0YDQvD0iNC4wMiI+PNCY0LTQl9Cw0L/RgNC+0YHQpD41NzgzNDwv0JjQtNCX0LDQv9GA0L7RgdCkPjwv0JTQvtC60YPQvNC10L3Rgj4KPC9zbWV2OkFwcERhdGE+PC9zbWV2Ok1lc3NhZ2VEYXRhPgo8L3dzOlNlbmRSZXF1ZXN0UnM+PC9TOkJvZHk+PC9TOkVudmVsb3BlPg==";
		String sbase64 = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+CjxTOkVudmVsb3BlIHhtbG5zOlM9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIiB4bWxuczp3c3NlPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktc2VjZXh0LTEuMC54c2QiIHhtbG5zOndzdT0iaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3NzLzIwMDQvMDEvb2FzaXMtMjAwNDAxLXdzcy13c3NlY3VyaXR5LXV0aWxpdHktMS4wLnhzZCI+PFM6SGVhZGVyPjx3c3NlOlNlY3VyaXR5IFM6YWN0b3I9Imh0dHA6Ly9zbWV2Lmdvc3VzbHVnaS5ydS9hY3RvcnMvc21ldiI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpTaWduZWRJbmZvPgo8ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPgo8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjZ29zdHIzNDEwMjAwMS1nb3N0cjM0MTEiLz4KPGRzOlJlZmVyZW5jZSBVUkk9IiNib2R5Ij4KPGRzOlRyYW5zZm9ybXM+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPgo8ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+CjwvZHM6VHJhbnNmb3Jtcz4KPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI2dvc3RyMzQxMSIvPgo8ZHM6RGlnZXN0VmFsdWU+M2lFemxGVTdWZm5SKzFxSzVHQUtKdTd6VmM0R3pCR1VHd3pwYm0yZWF2UT08L2RzOkRpZ2VzdFZhbHVlPgo8L2RzOlJlZmVyZW5jZT4KPC9kczpTaWduZWRJbmZvPgo8ZHM6U2lnbmF0dXJlVmFsdWU+CmFMallYVDVOUnNNakZpeUQ1ZEE5TDFXS0tuMFlPMXVTVVVYb2dIcmtrOHlKZUFoREdrZ0FobkErM3VqT0c3bzJoclc4ZW4ydEZCV3oKQ3FzSE5sREdodz09CjwvZHM6U2lnbmF0dXJlVmFsdWU+CjxkczpLZXlJbmZvPgo8d3NzZTpTZWN1cml0eVRva2VuUmVmZXJlbmNlPjx3c3NlOlJlZmVyZW5jZSBVUkk9IiNGTlNDZXJ0aWZpY2F0ZSIvPjwvd3NzZTpTZWN1cml0eVRva2VuUmVmZXJlbmNlPgo8L2RzOktleUluZm8+CjwvZHM6U2lnbmF0dXJlPjx3c3NlOkJpbmFyeVNlY3VyaXR5VG9rZW4gRW5jb2RpbmdUeXBlPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXNvYXAtbWVzc2FnZS1zZWN1cml0eS0xLjAjQmFzZTY0QmluYXJ5IiBWYWx1ZVR5cGU9Imh0dHA6Ly9kb2NzLm9hc2lzLW9wZW4ub3JnL3dzcy8yMDA0LzAxL29hc2lzLTIwMDQwMS13c3MteDUwOS10b2tlbi1wcm9maWxlLTEuMCNYNTA5djMiIHdzdTpJZD0iRk5TQ2VydGlmaWNhdGUiPk1JSUcwekNDQm9LZ0F3SUJBZ0lLR3dlQUpnQUFBQUFCU2pBSUJnWXFoUU1DQWdNd2dnRXhNUmd3RmdZRktvVURaQUVURFRFeU16UTFOamM0T1RBeE1qTXhHREFXQmdncWhRTURnUU1CQVJNS01USXpORFUyTnpnNU1ERW9NQ1lHQTFVRUNRd2YwS0hSZzlHSjBMWFFzdEdCMExyUXVOQzVJTkN5MExEUXV5RFF0QzR5TmpFWE1CVUdDU3FHU0liM0RRRUpBUllJWTJGQWNuUXVjblV4Q3pBSkJnTlZCQVlUQWxKVk1SVXdFd1lEVlFRSURBelFuTkMrMFlIUXV0Q3kwTEF4RlRBVEJnTlZCQWNNRE5DYzBMN1JnZEM2MExMUXNERWtNQ0lHQTFVRUNnd2IwSjdRa05DZUlOQ2cwTDdSZ2RHQzBMWFF1OUMxMExyUXZ0QzhNVEF3TGdZRFZRUUxEQ2ZRbzlDMDBMN1JnZEdDMEw3UXN0QzEwWURSajlHTzBZblF1TkM1SU5HRzBMWFF2ZEdDMFlBeEpUQWpCZ05WQkFNTUhOQ2kwTFhSZ2RHQzBMN1FzdEdMMExrZzBLUFFwaURRb05DaTBKb3dIaGNOTVRJd056QXpNVE13TWpBd1doY05NVE13TnpBek1UTXhNVEF3V2pDQm9URUxNQWtHQTFVRUJoTUNVbFV4YXpCcEJnTlZCQW9lWWdRa0JEVUVOQVExQkVBRU1BUTdCRXdFUFFRd0JFOEFJQVFkQkRBRU93UStCRE1FUGdReUJEQUVUd0FnQkNFRU93UkRCRFlFTVFRd0FDQUVJQVErQkVFRVFRUTRCRGtFUVFRNkJENEVPUUFnQkNRRU5RUTBCRFVFUUFRd0JFWUVPQVE0TVNVd0l3WURWUVFESGh3RUlRUWNCQklBSUFRa0JCMEVJUUFnQkNBRVBnUkJCRUVFT0FRNE1HTXdIQVlHS29VREFnSVRNQklHQnlxRkF3SUNKQUFHQnlxRkF3SUNIZ0VEUXdBRVFJQUc1M0dyL2NYYTFrZkxaa2FDUjdBMXU2VmdXRHF0MzZHcDZmcVIvMVR6aEx3dWluRmpKdjJXYVR3S3FTdTUyR3M4R0ovWmt0SS9BQVBxV1MvMDhocWpnZ1FGTUlJRUFUQU9CZ05WSFE4QkFmOEVCQU1DQlBBd0pBWURWUjBsQkIwd0d3WUdLb1VEWkFJQ0JnY3FoUU1DQWlJR0JnZ3JCZ0VGQlFjREFqQWRCZ05WSFE0RUZnUVVHK1lzUWRrSThHSkwwd3hFeTh1NUtiSU5laVV3Z2dGeUJnTlZIU01FZ2dGcE1JSUJaWUFVR3F6c3FKcXlNRXRuWnVXckxDWHFxd3FCd2RPaGdnRTVwSUlCTlRDQ0FURXhHREFXQmdVcWhRTmtBUk1OTVRJek5EVTJOemc1TURFeU16RVlNQllHQ0NxRkF3T0JBd0VCRXdveE1qTTBOVFkzT0Rrd01TZ3dKZ1lEVlFRSkRCL1FvZEdEMFluUXRkQ3kwWUhRdXRDNDBMa2cwTExRc05DN0lOQzBMakkyTVJjd0ZRWUpLb1pJaHZjTkFRa0JGZ2hqWVVCeWRDNXlkVEVMTUFrR0ExVUVCaE1DVWxVeEZUQVRCZ05WQkFnTUROQ2MwTDdSZ2RDNjBMTFFzREVWTUJNR0ExVUVCd3dNMEp6UXZ0R0IwTHJRc3RDd01TUXdJZ1lEVlFRS0RCdlFudENRMEo0ZzBLRFF2dEdCMFlMUXRkQzcwTFhRdXRDKzBMd3hNREF1QmdOVkJBc01KOUNqMExUUXZ0R0IwWUxRdnRDeTBMWFJnTkdQMFk3UmlkQzQwTGtnMFliUXRkQzkwWUxSZ0RFbE1DTUdBMVVFQXd3YzBLTFF0ZEdCMFlMUXZ0Q3kwWXZRdVNEUW85Q21JTkNnMEtMUW1vSVFZa3hIYnpnVWNxWkd2M0RMRFI0MEd6QlpCZ05WSFI4RVVqQlFNRTZnVEtCS2hraG9kSFJ3T2k4dk1UZzRMakkxTkM0eE5pNDRPUzl5WVM5alpIQXZNV0ZoWTJWallUZzVZV0l5TXpBMFlqWTNOalpsTldGaU1tTXlOV1ZoWVdJd1lUZ3hZekZrTXk1amNtd3dSd1lJS3dZQkJRVUhBUUVFT3pBNU1EY0dDQ3NHQVFVRkJ6QUNoaXRvZEhSd09pOHZNVGc0TGpJMU5DNHhOaTQ0T1M5eVlTOWpaSEF2ZEdWemRGOWpZVjl5ZEdzdVkzSjBNRThHQlNxRkEyUnZCRVlNUkNMUW10R0EwTGpRdjlHQzBMN1FuOUdBMEw0Z1ExTlFJaUFvMExMUXRkR0EwWUhRdU5HUElETXVOaWtnS05DNDBZSFF2OUMrMEx2UXZkQzEwTDNRdU5DMUlERXBNQ3NHQTFVZEVBUWtNQ0tBRHpJd01USXdOekF6TVRNd01qQXdXb0VQTWpBeE16QTNNRE14TXpBeU1EQmFNQ29HQTFVZElBUWpNQ0V3Q0FZR0tvVURaQUlDTUFrR0J5cUZBd0lDSWdZd0NnWUlLd1lCQlFVSEF3SXdnZVVHQlNxRkEyUndCSUhiTUlIWURDc2kwSnJSZ05DNDBML1JndEMrMEovUmdOQytJRU5UVUNJZ0tOQ3kwTFhSZ05HQjBMalJqeUF6TGpZcERGUWkwS1BRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSUNMUW10R0EwTGpRdjlHQzBMN1FuOUdBMEw0ZzBLUFFwaUlpSU5DeTBMWFJnTkdCMExqUmp5QXhMalVNTE5DaDBLUXZNVEkwTFRFMU5ETWcwTDdSZ2lBMElOQyswTHJSZ3RHUDBMSFJnTkdQSURJd01UQWcwTE11RENYUW9kQ2tMekV5T0MweE5qVTRJTkMrMFlJZ01ERWcwTHpRc05HUElESXdNVEVnMExNdU1BZ0dCaXFGQXdJQ0F3TkJBTEdMaEdFQVBPMmU5RjdicFBLOHhmWkk0TDlsanlPR1RjMG9SNjJnK1JoMFdaRDQrMy8vS0xMTFNhMU4xVmU3eXkwZmxtTEFYeFFMVzFKVGx6ZnU4UEk9PC93c3NlOkJpbmFyeVNlY3VyaXR5VG9rZW4+PC93c3NlOlNlY3VyaXR5PjwvUzpIZWFkZXI+PFM6Qm9keSB3c3U6SWQ9ImJvZHkiPjx3czpTZW5kUmVxdWVzdFJzIHhtbG5zOndzPSJodHRwOi8vd3MudW5pc29mdC8iIHhtbG5zOlM9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIj4KPHNtZXY6TWVzc2FnZSB4bWxuczpzbWV2PSJodHRwOi8vc21ldi5nb3N1c2x1Z2kucnUvcmV2MTExMTExIj48c21ldjpTZW5kZXI+PHNtZXY6Q29kZT5GTlMwMDEwMDE8L3NtZXY6Q29kZT48c21ldjpOYW1lPtCk0J3QoSDQoNC+0YHRgdC40Lg8L3NtZXY6TmFtZT48L3NtZXY6U2VuZGVyPjxzbWV2OlJlY2lwaWVudD48c21ldjpDb2RlPjEzMjQyPC9zbWV2OkNvZGU+PHNtZXY6TmFtZT5NSU5TVllBWl9TWVNfMTwvc21ldjpOYW1lPjwvc21ldjpSZWNpcGllbnQ+PHNtZXY6T3JpZ2luYXRvcj48c21ldjpDb2RlPjEzMjQyPC9zbWV2OkNvZGU+PHNtZXY6TmFtZT5NSU5TVllBWl9TWVNfMTwvc21ldjpOYW1lPjwvc21ldjpPcmlnaW5hdG9yPjxzbWV2OlR5cGVDb2RlPkdTUlY8L3NtZXY6VHlwZUNvZGU+PHNtZXY6U3RhdHVzPkFDQ0VQVDwvc21ldjpTdGF0dXM+PHNtZXY6RGF0ZT4yMDEyLTEwLTI1VDEwOjQ4OjQzWjwvc21ldjpEYXRlPjxzbWV2OkV4Y2hhbmdlVHlwZT4yPC9zbWV2OkV4Y2hhbmdlVHlwZT48c21ldjpTZXJ2aWNlQ29kZT5TSUQwMDAzNDUxPC9zbWV2OlNlcnZpY2VDb2RlPjxzbWV2OkNhc2VOdW1iZXI+ODkxNjwvc21ldjpDYXNlTnVtYmVyPjxzbWV2OlRlc3RNc2cvPjwvc21ldjpNZXNzYWdlPjxzbWV2Ok1lc3NhZ2VEYXRhIHhtbG5zOnNtZXY9Imh0dHA6Ly9zbWV2Lmdvc3VzbHVnaS5ydS9yZXYxMTExMTEiPjxzbWV2OkFwcERhdGEgd3N1OklkPSJmbnMtQXBwRGF0YSIgeG1sbnM6d3N1PSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIj4KPNCU0L7QutGD0LzQtdC90YIgeG1sbnM9Imh0dHA6Ly93cy51bmlzb2Z0L0ZOU1pETC9SczEiINCS0LXRgNGB0KTQvtGA0Lw9IjQuMDIiPjzQmNC00JfQsNC/0YDQvtGB0KQ+NTc4MzQ8L9CY0LTQl9Cw0L/RgNC+0YHQpD48L9CU0L7QutGD0LzQtdC90YI+Cjwvc21ldjpBcHBEYXRhPjwvc21ldjpNZXNzYWdlRGF0YT4KPC93czpTZW5kUmVxdWVzdFJzPjwvUzpCb2R5PjwvUzpFbnZlbG9wZT4=";
		SOAPMessage ms = getSOAPMessageFromString(new String(Base64.decode(sbase64.getBytes())));
		
		boolean bres = verifySecuredMessage(ms, false);
		Assert.assertTrue(bres);
		
	}
	
//	@Test
	public void testCanon() throws Exception 
	{
		String sxmlUnsigned = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
			      "<SOAP-ENV:Envelope" + " xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
			      "<SOAP-ENV:Body>" +
			      "    <sayHello xmlns=\"http://jeffhanson.com/services/helloworld\">" +
			      "        <value xmlns=\"\">Hello world!</value>" +
			      "        <aaa/>" +
			      "    </sayHello>" +
			      "</SOAP-ENV:Body>" +
			      "</SOAP-ENV:Envelope>";
		Document doc = getDocumentFromString(sxmlUnsigned);
		
		byte[] bcan = getCanonXML(doc);
		String scan = new String(bcan);
	
		System.out.println();
		System.out.println(scan);
		System.out.println();
	
	}
	
//	@Test
	public void testCertBC() throws Exception 
	{
		byte[] b64cert = FileUtils.readFileToByteArray(new File("/home/irina/v/workspace/xmldsigri_gost/data/adm/cert.base64"));
		
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		Certificate xcert = cf.generateCertificate(new ByteArrayInputStream(Base64.decode(b64cert)));

		byte[] b64key = FileUtils.readFileToByteArray(new File("/home/irina/v/workspace/xmldsigri_gost/data/adm/keyBC.base64"));
		
		KeyFactory keyFac8 = KeyFactory.getInstance("GOST3410", "BC");
		PKCS8EncodedKeySpec pvks = new PKCS8EncodedKeySpec(Base64.decode(b64key));
		PrivateKey pvk1 = keyFac8.generatePrivate(pvks);
		
		String salg = pvk1.getAlgorithm();
		String senc = new String( Base64.encode(pvk1.getEncoded()) );

		System.out.println();
		System.out.println(senc);
		System.out.println();
		
	}
	
//	@Test
	public void testDigest() throws Exception 
	{
//		String sbase64 = "PFM6Qm9keSB3c3U6SWQ9ImJvZHkiPjx3czpTZW5kUmVxdWVzdFJzIHhtbG5zOndzPSJodHRwOi8vd3MudW5pc29mdC8iIHhtbG5zOlM9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIj4KPHNtZXY6TWVzc2FnZSB4bWxuczpzbWV2PSJodHRwOi8vc21ldi5nb3N1c2x1Z2kucnUvcmV2MTExMTExIj48c21ldjpTZW5kZXI+PHNtZXY6Q29kZT5GTlMwMDEwMDE8L3NtZXY6Q29kZT48c21ldjpOYW1lPtCk0J3QoSDQoNC+0YHRgdC40Lg8L3NtZXY6TmFtZT48L3NtZXY6U2VuZGVyPjxzbWV2OlJlY2lwaWVudD48c21ldjpDb2RlPjEzMjQyPC9zbWV2OkNvZGU+PHNtZXY6TmFtZT5NSU5TVllBWl9TWVNfMTwvc21ldjpOYW1lPjwvc21ldjpSZWNpcGllbnQ+PHNtZXY6T3JpZ2luYXRvcj48c21ldjpDb2RlPjEzMjQyPC9zbWV2OkNvZGU+PHNtZXY6TmFtZT5NSU5TVllBWl9TWVNfMTwvc21ldjpOYW1lPjwvc21ldjpPcmlnaW5hdG9yPjxzbWV2OlR5cGVDb2RlPkdTUlY8L3NtZXY6VHlwZUNvZGU+PHNtZXY6U3RhdHVzPkFDQ0VQVDwvc21ldjpTdGF0dXM+PHNtZXY6RGF0ZT4yMDEyLTEwLTI1VDEwOjQ4OjQzWjwvc21ldjpEYXRlPjxzbWV2OkV4Y2hhbmdlVHlwZT4yPC9zbWV2OkV4Y2hhbmdlVHlwZT48c21ldjpTZXJ2aWNlQ29kZT5TSUQwMDAzNDUxPC9zbWV2OlNlcnZpY2VDb2RlPjxzbWV2OkNhc2VOdW1iZXI+ODkxNjwvc21ldjpDYXNlTnVtYmVyPjxzbWV2OlRlc3RNc2cvPjwvc21ldjpNZXNzYWdlPjxzbWV2Ok1lc3NhZ2VEYXRhIHhtbG5zOnNtZXY9Imh0dHA6Ly9zbWV2Lmdvc3VzbHVnaS5ydS9yZXYxMTExMTEiPjxzbWV2OkFwcERhdGEgd3N1OklkPSJmbnMtQXBwRGF0YSIgeG1sbnM6d3N1PSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIj4KPNCU0L7QutGD0LzQtdC90YIgeG1sbnM9Imh0dHA6Ly93cy51bmlzb2Z0L0ZOU1pETC9SczEiINCS0LXRgNGB0KTQvtGA0Lw9IjQuMDIiPjzQmNC00JfQsNC/0YDQvtGB0KQ+NTc4MzQ8L9CY0LTQl9Cw0L/RgNC+0YHQpD48L9CU0L7QutGD0LzQtdC90YI+Cjwvc21ldjpBcHBEYXRhPjwvc21ldjpNZXNzYWdlRGF0YT4KPC93czpTZW5kUmVxdWVzdFJzPjwvUzpCb2R5Pg==";		
//		String sfdig64 = new String(Base64.decode(sbase64.getBytes()));
		String sfdig64 = "<S:Body xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\" wsu:Id=\"body\"><ws:SendRequestRs xmlns:ws=\"http://ws.unisoft/\"><smev:Message xmlns:smev=\"http://smev.gosuslugi.ru/rev111111\"><smev:Sender><smev:Code>FNS001001</smev:Code><smev:Name>ФНС России</smev:Name></smev:Sender><smev:Recipient><smev:Code>13242</smev:Code><smev:Name>MINSVYAZ_SYS_1</smev:Name></smev:Recipient><smev:Originator><smev:Code>13242</smev:Code><smev:Name>MINSVYAZ_SYS_1</smev:Name></smev:Originator><smev:TypeCode>GSRV</smev:TypeCode><smev:Status>ACCEPT</smev:Status><smev:Date>2012-10-25T10:48:43Z</smev:Date><smev:ExchangeType>2</smev:ExchangeType><smev:ServiceCode>SID0003451</smev:ServiceCode><smev:CaseNumber>8916</smev:CaseNumber><smev:TestMsg/></smev:Message><smev:MessageData xmlns:smev=\"http://smev.gosuslugi.ru/rev111111\"><smev:AppData wsu:Id=\"fns-AppData\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><Документ xmlns=\"http://ws.unisoft/FNSZDL/Rs1\" ВерсФорм=\"4.02\"><ИдЗапросФ>57834</ИдЗапросФ></Документ></smev:AppData></smev:MessageData></ws:SendRequestRs></S:Body>";
		System.out.println();
		System.out.println(sfdig64);
		System.out.println();			
		
		MessageDigest md = MessageDigest.getInstance("GOST3411", "BC");
		md.update(sfdig64.getBytes());
		byte[] bdig = md.digest();

		System.out.println();
		System.out.println(Base64.encode(bdig));
		System.out.println();		
		
		md = MessageDigest.getInstance("GOST3411", "SC");
		md.update(sfdig64.getBytes());
		bdig = md.digest();

		System.out.println();
		System.out.println(Base64.encode(bdig));
		System.out.println();
		
		md = MessageDigest.getInstance("GOST3411", "JCP");
		md.update(sfdig64.getBytes());
		bdig = md.digest();

		System.out.println();
		System.out.println(Base64.encode(bdig));
		System.out.println();	
		
		// 3iEzlFU7VfnR+1qK5GAKJu7zVc4GzBGUGwzpbm2eavQ=
		
	}
	
//	@Test
	public void testCanon2() throws Exception 
	{
		String sbase64 = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+CjxTOkVudmVsb3BlIHhtbG5zOlM9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIiB4bWxuczp3c3NlPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktc2VjZXh0LTEuMC54c2QiIHhtbG5zOndzdT0iaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3NzLzIwMDQvMDEvb2FzaXMtMjAwNDAxLXdzcy13c3NlY3VyaXR5LXV0aWxpdHktMS4wLnhzZCI+PFM6SGVhZGVyPjx3c3NlOlNlY3VyaXR5IFM6YWN0b3I9Imh0dHA6Ly9zbWV2Lmdvc3VzbHVnaS5ydS9hY3RvcnMvc21ldiI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpTaWduZWRJbmZvPgo8ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPgo8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjZ29zdHIzNDEwMjAwMS1nb3N0cjM0MTEiLz4KPGRzOlJlZmVyZW5jZSBVUkk9IiNib2R5Ij4KPGRzOlRyYW5zZm9ybXM+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPgo8ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+CjwvZHM6VHJhbnNmb3Jtcz4KPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI2dvc3RyMzQxMSIvPgo8ZHM6RGlnZXN0VmFsdWU+M2lFemxGVTdWZm5SKzFxSzVHQUtKdTd6VmM0R3pCR1VHd3pwYm0yZWF2UT08L2RzOkRpZ2VzdFZhbHVlPgo8L2RzOlJlZmVyZW5jZT4KPC9kczpTaWduZWRJbmZvPgo8ZHM6U2lnbmF0dXJlVmFsdWU+CmFMallYVDVOUnNNakZpeUQ1ZEE5TDFXS0tuMFlPMXVTVVVYb2dIcmtrOHlKZUFoREdrZ0FobkErM3VqT0c3bzJoclc4ZW4ydEZCV3oKQ3FzSE5sREdodz09CjwvZHM6U2lnbmF0dXJlVmFsdWU+CjxkczpLZXlJbmZvPgo8d3NzZTpTZWN1cml0eVRva2VuUmVmZXJlbmNlPjx3c3NlOlJlZmVyZW5jZSBVUkk9IiNGTlNDZXJ0aWZpY2F0ZSIvPjwvd3NzZTpTZWN1cml0eVRva2VuUmVmZXJlbmNlPgo8L2RzOktleUluZm8+CjwvZHM6U2lnbmF0dXJlPjx3c3NlOkJpbmFyeVNlY3VyaXR5VG9rZW4gRW5jb2RpbmdUeXBlPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXNvYXAtbWVzc2FnZS1zZWN1cml0eS0xLjAjQmFzZTY0QmluYXJ5IiBWYWx1ZVR5cGU9Imh0dHA6Ly9kb2NzLm9hc2lzLW9wZW4ub3JnL3dzcy8yMDA0LzAxL29hc2lzLTIwMDQwMS13c3MteDUwOS10b2tlbi1wcm9maWxlLTEuMCNYNTA5djMiIHdzdTpJZD0iRk5TQ2VydGlmaWNhdGUiPk1JSUcwekNDQm9LZ0F3SUJBZ0lLR3dlQUpnQUFBQUFCU2pBSUJnWXFoUU1DQWdNd2dnRXhNUmd3RmdZRktvVURaQUVURFRFeU16UTFOamM0T1RBeE1qTXhHREFXQmdncWhRTURnUU1CQVJNS01USXpORFUyTnpnNU1ERW9NQ1lHQTFVRUNRd2YwS0hSZzlHSjBMWFFzdEdCMExyUXVOQzVJTkN5MExEUXV5RFF0QzR5TmpFWE1CVUdDU3FHU0liM0RRRUpBUllJWTJGQWNuUXVjblV4Q3pBSkJnTlZCQVlUQWxKVk1SVXdFd1lEVlFRSURBelFuTkMrMFlIUXV0Q3kwTEF4RlRBVEJnTlZCQWNNRE5DYzBMN1JnZEM2MExMUXNERWtNQ0lHQTFVRUNnd2IwSjdRa05DZUlOQ2cwTDdSZ2RHQzBMWFF1OUMxMExyUXZ0QzhNVEF3TGdZRFZRUUxEQ2ZRbzlDMDBMN1JnZEdDMEw3UXN0QzEwWURSajlHTzBZblF1TkM1SU5HRzBMWFF2ZEdDMFlBeEpUQWpCZ05WQkFNTUhOQ2kwTFhSZ2RHQzBMN1FzdEdMMExrZzBLUFFwaURRb05DaTBKb3dIaGNOTVRJd056QXpNVE13TWpBd1doY05NVE13TnpBek1UTXhNVEF3V2pDQm9URUxNQWtHQTFVRUJoTUNVbFV4YXpCcEJnTlZCQW9lWWdRa0JEVUVOQVExQkVBRU1BUTdCRXdFUFFRd0JFOEFJQVFkQkRBRU93UStCRE1FUGdReUJEQUVUd0FnQkNFRU93UkRCRFlFTVFRd0FDQUVJQVErQkVFRVFRUTRCRGtFUVFRNkJENEVPUUFnQkNRRU5RUTBCRFVFUUFRd0JFWUVPQVE0TVNVd0l3WURWUVFESGh3RUlRUWNCQklBSUFRa0JCMEVJUUFnQkNBRVBnUkJCRUVFT0FRNE1HTXdIQVlHS29VREFnSVRNQklHQnlxRkF3SUNKQUFHQnlxRkF3SUNIZ0VEUXdBRVFJQUc1M0dyL2NYYTFrZkxaa2FDUjdBMXU2VmdXRHF0MzZHcDZmcVIvMVR6aEx3dWluRmpKdjJXYVR3S3FTdTUyR3M4R0ovWmt0SS9BQVBxV1MvMDhocWpnZ1FGTUlJRUFUQU9CZ05WSFE4QkFmOEVCQU1DQlBBd0pBWURWUjBsQkIwd0d3WUdLb1VEWkFJQ0JnY3FoUU1DQWlJR0JnZ3JCZ0VGQlFjREFqQWRCZ05WSFE0RUZnUVVHK1lzUWRrSThHSkwwd3hFeTh1NUtiSU5laVV3Z2dGeUJnTlZIU01FZ2dGcE1JSUJaWUFVR3F6c3FKcXlNRXRuWnVXckxDWHFxd3FCd2RPaGdnRTVwSUlCTlRDQ0FURXhHREFXQmdVcWhRTmtBUk1OTVRJek5EVTJOemc1TURFeU16RVlNQllHQ0NxRkF3T0JBd0VCRXdveE1qTTBOVFkzT0Rrd01TZ3dKZ1lEVlFRSkRCL1FvZEdEMFluUXRkQ3kwWUhRdXRDNDBMa2cwTExRc05DN0lOQzBMakkyTVJjd0ZRWUpLb1pJaHZjTkFRa0JGZ2hqWVVCeWRDNXlkVEVMTUFrR0ExVUVCaE1DVWxVeEZUQVRCZ05WQkFnTUROQ2MwTDdSZ2RDNjBMTFFzREVWTUJNR0ExVUVCd3dNMEp6UXZ0R0IwTHJRc3RDd01TUXdJZ1lEVlFRS0RCdlFudENRMEo0ZzBLRFF2dEdCMFlMUXRkQzcwTFhRdXRDKzBMd3hNREF1QmdOVkJBc01KOUNqMExUUXZ0R0IwWUxRdnRDeTBMWFJnTkdQMFk3UmlkQzQwTGtnMFliUXRkQzkwWUxSZ0RFbE1DTUdBMVVFQXd3YzBLTFF0ZEdCMFlMUXZ0Q3kwWXZRdVNEUW85Q21JTkNnMEtMUW1vSVFZa3hIYnpnVWNxWkd2M0RMRFI0MEd6QlpCZ05WSFI4RVVqQlFNRTZnVEtCS2hraG9kSFJ3T2k4dk1UZzRMakkxTkM0eE5pNDRPUzl5WVM5alpIQXZNV0ZoWTJWallUZzVZV0l5TXpBMFlqWTNOalpsTldGaU1tTXlOV1ZoWVdJd1lUZ3hZekZrTXk1amNtd3dSd1lJS3dZQkJRVUhBUUVFT3pBNU1EY0dDQ3NHQVFVRkJ6QUNoaXRvZEhSd09pOHZNVGc0TGpJMU5DNHhOaTQ0T1M5eVlTOWpaSEF2ZEdWemRGOWpZVjl5ZEdzdVkzSjBNRThHQlNxRkEyUnZCRVlNUkNMUW10R0EwTGpRdjlHQzBMN1FuOUdBMEw0Z1ExTlFJaUFvMExMUXRkR0EwWUhRdU5HUElETXVOaWtnS05DNDBZSFF2OUMrMEx2UXZkQzEwTDNRdU5DMUlERXBNQ3NHQTFVZEVBUWtNQ0tBRHpJd01USXdOekF6TVRNd01qQXdXb0VQTWpBeE16QTNNRE14TXpBeU1EQmFNQ29HQTFVZElBUWpNQ0V3Q0FZR0tvVURaQUlDTUFrR0J5cUZBd0lDSWdZd0NnWUlLd1lCQlFVSEF3SXdnZVVHQlNxRkEyUndCSUhiTUlIWURDc2kwSnJSZ05DNDBML1JndEMrMEovUmdOQytJRU5UVUNJZ0tOQ3kwTFhSZ05HQjBMalJqeUF6TGpZcERGUWkwS1BRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSUNMUW10R0EwTGpRdjlHQzBMN1FuOUdBMEw0ZzBLUFFwaUlpSU5DeTBMWFJnTkdCMExqUmp5QXhMalVNTE5DaDBLUXZNVEkwTFRFMU5ETWcwTDdSZ2lBMElOQyswTHJSZ3RHUDBMSFJnTkdQSURJd01UQWcwTE11RENYUW9kQ2tMekV5T0MweE5qVTRJTkMrMFlJZ01ERWcwTHpRc05HUElESXdNVEVnMExNdU1BZ0dCaXFGQXdJQ0F3TkJBTEdMaEdFQVBPMmU5RjdicFBLOHhmWkk0TDlsanlPR1RjMG9SNjJnK1JoMFdaRDQrMy8vS0xMTFNhMU4xVmU3eXkwZmxtTEFYeFFMVzFKVGx6ZnU4UEk9PC93c3NlOkJpbmFyeVNlY3VyaXR5VG9rZW4+PC93c3NlOlNlY3VyaXR5PjwvUzpIZWFkZXI+PFM6Qm9keSB3c3U6SWQ9ImJvZHkiPjx3czpTZW5kUmVxdWVzdFJzIHhtbG5zOndzPSJodHRwOi8vd3MudW5pc29mdC8iIHhtbG5zOlM9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIj4KPHNtZXY6TWVzc2FnZSB4bWxuczpzbWV2PSJodHRwOi8vc21ldi5nb3N1c2x1Z2kucnUvcmV2MTExMTExIj48c21ldjpTZW5kZXI+PHNtZXY6Q29kZT5GTlMwMDEwMDE8L3NtZXY6Q29kZT48c21ldjpOYW1lPtCk0J3QoSDQoNC+0YHRgdC40Lg8L3NtZXY6TmFtZT48L3NtZXY6U2VuZGVyPjxzbWV2OlJlY2lwaWVudD48c21ldjpDb2RlPjEzMjQyPC9zbWV2OkNvZGU+PHNtZXY6TmFtZT5NSU5TVllBWl9TWVNfMTwvc21ldjpOYW1lPjwvc21ldjpSZWNpcGllbnQ+PHNtZXY6T3JpZ2luYXRvcj48c21ldjpDb2RlPjEzMjQyPC9zbWV2OkNvZGU+PHNtZXY6TmFtZT5NSU5TVllBWl9TWVNfMTwvc21ldjpOYW1lPjwvc21ldjpPcmlnaW5hdG9yPjxzbWV2OlR5cGVDb2RlPkdTUlY8L3NtZXY6VHlwZUNvZGU+PHNtZXY6U3RhdHVzPkFDQ0VQVDwvc21ldjpTdGF0dXM+PHNtZXY6RGF0ZT4yMDEyLTEwLTI1VDEwOjQ4OjQzWjwvc21ldjpEYXRlPjxzbWV2OkV4Y2hhbmdlVHlwZT4yPC9zbWV2OkV4Y2hhbmdlVHlwZT48c21ldjpTZXJ2aWNlQ29kZT5TSUQwMDAzNDUxPC9zbWV2OlNlcnZpY2VDb2RlPjxzbWV2OkNhc2VOdW1iZXI+ODkxNjwvc21ldjpDYXNlTnVtYmVyPjxzbWV2OlRlc3RNc2cvPjwvc21ldjpNZXNzYWdlPjxzbWV2Ok1lc3NhZ2VEYXRhIHhtbG5zOnNtZXY9Imh0dHA6Ly9zbWV2Lmdvc3VzbHVnaS5ydS9yZXYxMTExMTEiPjxzbWV2OkFwcERhdGEgd3N1OklkPSJmbnMtQXBwRGF0YSIgeG1sbnM6d3N1PSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIj4KPNCU0L7QutGD0LzQtdC90YIgeG1sbnM9Imh0dHA6Ly93cy51bmlzb2Z0L0ZOU1pETC9SczEiINCS0LXRgNGB0KTQvtGA0Lw9IjQuMDIiPjzQmNC00JfQsNC/0YDQvtGB0KQ+NTc4MzQ8L9CY0LTQl9Cw0L/RgNC+0YHQpD48L9CU0L7QutGD0LzQtdC90YI+Cjwvc21ldjpBcHBEYXRhPjwvc21ldjpNZXNzYWdlRGF0YT4KPC93czpTZW5kUmVxdWVzdFJzPjwvUzpCb2R5PjwvUzpFbnZlbG9wZT4=";		
		String sxmlUnsigned = new String(Base64.decode(sbase64.getBytes()));
		System.out.println();
		System.out.println(sxmlUnsigned);
		System.out.println();		
		
		Document doc = getDocumentFromString(sxmlUnsigned);
		
		byte[] bcan = getCanonXML(doc);
		String scan = new String(bcan);
	
		System.out.println();
		System.out.println(scan);
		System.out.println();
	
		String sfdig64 = "<S:Body xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\" wsu:Id=\"body\"><ws:SendRequestRs xmlns:ws=\"http://ws.unisoft/\"><smev:Message xmlns:smev=\"http://smev.gosuslugi.ru/rev111111\"><smev:Sender><smev:Code>FNS001001</smev:Code><smev:Name>ФНС России</smev:Name></smev:Sender><smev:Recipient><smev:Code>13242</smev:Code><smev:Name>MINSVYAZ_SYS_1</smev:Name></smev:Recipient><smev:Originator><smev:Code>13242</smev:Code><smev:Name>MINSVYAZ_SYS_1</smev:Name></smev:Originator><smev:TypeCode>GSRV</smev:TypeCode><smev:Status>ACCEPT</smev:Status><smev:Date>2012-10-25T10:48:43Z</smev:Date><smev:ExchangeType>2</smev:ExchangeType><smev:ServiceCode>SID0003451</smev:ServiceCode><smev:CaseNumber>8916</smev:CaseNumber><smev:TestMsg/></smev:Message><smev:MessageData xmlns:smev=\"http://smev.gosuslugi.ru/rev111111\"><smev:AppData wsu:Id=\"fns-AppData\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><Документ xmlns=\"http://ws.unisoft/FNSZDL/Rs1\" ВерсФорм=\"4.02\"><ИдЗапросФ>57834</ИдЗапросФ></Документ></smev:AppData></smev:MessageData></ws:SendRequestRs></S:Body>";
		
		MessageDigest md = MessageDigest.getInstance("GOST3411", "BC");
		md.update(sfdig64.getBytes());
		byte[] bdig = md.digest();

		System.out.println();
		System.out.println(Base64.encode(bdig));
		System.out.println();
		
	}
	
	
    public static byte[] getCanonXML(Node xmlContent) throws InvalidCanonicalizerException, CanonicalizationException 
    {
        
        Canonicalizer canon;
        byte[] canon_xmlContent;

            canon = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
            canon_xmlContent = canon.canonicalizeSubtree(xmlContent);
            return canon_xmlContent;

    }	
	
	private boolean verifySecuredMessage(SOAPMessage message, boolean printCert) throws Exception {

		// Extract some nodes to verify document
		Document doc = message.getSOAPPart().getEnvelope().getOwnerDocument();
        final Element wssecontext = doc.createElementNS(null, "namespaceContext");
        wssecontext.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:"+"wsse".trim(), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
        NodeList secnodeList = XPathAPI.selectNodeList(doc.getDocumentElement(), "//wsse:Security");

        Element r = null;
        Element el = null;
        if( secnodeList != null&&secnodeList.getLength()>0 ) {
        	String actorAttr = null;
        	for( int i = 0; i<secnodeList.getLength(); i++ ) {
        		el = (Element) secnodeList.item(i);
        		actorAttr = el.getAttributeNS("http://schemas.xmlsoap.org/soap/envelope/", "actor");
        		if(actorAttr != null&&actorAttr.equals("http://smev.gosuslugi.ru/actors/smev")) {
        			r = (Element)XPathAPI.selectSingleNode(el, "//wsse:BinarySecurityToken[1]", wssecontext); 
        			break;
        		}
        	}
        }
        if(r == null)
        	return false;
        
        final X509Security x509 = new X509Security(r);
       	if(x509 == null)
       		return false;
       	
       	// Extract certificate
       	final X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(x509.getToken()));
       	
       	if (cert == null) {
    	    throw new Exception("Cannot find certificate to verify signature");
    	}
       	
       	// Printing of certificate if need
       	if (printCert) {
       		System.out.println(cert);
       	}
       	
       	// Get signature node
       	NodeList nl = doc.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature");
    	if (nl.getLength() == 0) {
    	    throw new Exception("Cannot find Signature element");
    	}
    	
    	XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM", xmlDSigProvider);
    	// Set public key
    	DOMValidateContext valContext = new DOMValidateContext(KeySelector.singletonKeySelector(cert.getPublicKey()), nl.item(0));
    	valContext.setProperty("org.jcp.xml.dsig.internal.dom.SignatureProvider", Security.getProvider("SC"));
    	valContext.setProperty("org.jcp.xml.dsig.internal.dom.SecureRandom", random);	
    	valContext.setProperty("org.jcp.xml.dsig.internal.dom.DigestProvider", Security.getProvider("BC"));
    	
    	javax.xml.crypto.dsig.XMLSignature signature = fac.unmarshalXMLSignature(valContext);
    	
		final Transforms transforms = new Transforms(doc);
		transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);    	
    	
    	// Verify signature
    	return signature.validate(valContext); 
	}	
	
	public boolean verifyDoc(String docStr, boolean printCert) 
	{
		
//		String docStr = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(signedDoc);
		InputStream inputStream = new StringBufferInputStream(docStr);
		MessageFactory messageFactory = null;
		SOAPMessage sm = null;
		boolean result = false;
		
		try {
			// Create SOAP XML message from string (signed document)
			messageFactory = MessageFactory.newInstance();
			sm = messageFactory.createMessage(null, inputStream);
			// Verify signature
			result = verifySecuredMessage(sm, printCert);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}	
	
	public static Document getDocumentFromString(String source) throws ParserConfigurationException, SAXException, IOException 
	{
		DocumentBuilderFactory dbuf = DocumentBuilderFactory.newInstance();
		DocumentBuilder dbu = dbuf.newDocumentBuilder();
		Document xdoc = dbu.parse(new ByteArrayInputStream(source.getBytes()));		
		return xdoc;
	}
	
	public static Document getDocumentFromBytes(byte[] source) throws ParserConfigurationException, SAXException, IOException 
	{
		DocumentBuilderFactory dbuf = DocumentBuilderFactory.newInstance();
		DocumentBuilder dbu = dbuf.newDocumentBuilder();
		Document xdoc = dbu.parse(new ByteArrayInputStream(source));		
		return xdoc;
	}	
	
	public Document signDoc(String docStr) {
		
		Document signedDoc = null;
		
		try {
			// Read only signed document
//			SOAPEnvelope envelope = getSOAPEnvelopeFromString(docStr);
//			signedDoc = envelope.getAsDocument();
			
			SOAPEnvelope envelope = getSOAPEnvelopeFromString(docStr);
			signedDoc = envelope.getOwnerDocument();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return signedDoc;
		
	}	

	public static KeyStore loadKeyStore( String storeType, File store, char[] storePassword ) 
	throws KeyStoreException, NoSuchAlgorithmException, CertificateException, 
		FileNotFoundException, IOException, NoSuchProviderException {

		KeyStore keyStore = KeyStore.getInstance(storeType, "SC");
		FileInputStream inputStream = null;

		if (store != null)
			inputStream = new FileInputStream(store);

		keyStore.load( inputStream, storePassword );
		return keyStore;		 
	}
	
	private void setSAMdata( File keyStore, char[] keyStorePass, String alias, char[] aliasKeyRecoveryPass) 
	throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, 
	IOException, UnrecoverableKeyException, NoSuchProviderException {
		
		// Load key store to extract certificate and key
		KeyStore ks = loadKeyStore("PKCS12", keyStore, keyStorePass);
		samData = new Object[]{(X509Certificate)ks.getCertificate(alias), ks.getKey(alias, aliasKeyRecoveryPass)};
	}
	
	private void constructSecuredMessage(SOAPMessage mf) throws GeneralSecurityException, 
	XMLSecurityException, SOAPException, ParserConfigurationException, TransformerException,
	WSSecurityException,FileNotFoundException,Exception {
		
		if (mf == null)
			return;
		// Prepare secured header
		mf.getSOAPPart().getEnvelope().addNamespaceDeclaration("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
		mf.getSOAPPart().getEnvelope().addNamespaceDeclaration("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
		mf.getSOAPPart().getEnvelope().addNamespaceDeclaration("ds", "http://www.w3.org/2000/09/xmldsig#");
		mf.getSOAPBody().setAttributeNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "wsu:Id","body");
		
		WSSecHeader header = new WSSecHeader();
		header.setActor("http://smev.gosuslugi.ru/actors/smev");
		header.setMustUnderstand(false);
		
		Element sec = header.insertSecurityHeader(mf.getSOAPPart());
		Document doc = mf.getSOAPPart().getEnvelope().getOwnerDocument();
		
		Element token =(Element) sec.appendChild(doc.createElementNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse:BinarySecurityToken")); 
		token.setAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
		token.setAttribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3");
		token.setAttribute("wsu:Id", "CertId");
		header.getSecurityHeader().appendChild(token);
		
		// Prepare signature provider
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM", xmlDSigProvider);

		final Transforms transforms = new Transforms(doc);
		transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);		
		
	    List<Transform> transformList = new ArrayList<Transform>();
	    Transform transform = fac.newTransform(Transform.ENVELOPED, (XMLStructure) null);
	    Transform transformC14N = fac.newTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS, (XMLStructure) null);
	    transformList.add(transform);
	    transformList.add(transformC14N);
	    
		Reference ref = fac.newReference("#body", fac.newDigestMethod("http://www.w3.org/2001/04/xmldsig-more#gostr3411", null), transformList, null, null);
		// Make link to signing element
		SignedInfo si = fac.newSignedInfo( fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, 
				 								(C14NMethodParameterSpec) null),
				 						   fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411", null),
				 						   		Collections.singletonList(ref));
		
		final Object[] obj = samData.clone();

		// Prepare key information to verify signature in future on other side
		KeyInfoFactory kif = fac.getKeyInfoFactory();
		X509Data x509d = kif.newX509Data(Collections.singletonList((X509Certificate) obj[0]));
		KeyInfo ki = kif.newKeyInfo(Collections.singletonList(x509d));
		
		// Create signature and sign by private key
		javax.xml.crypto.dsig.XMLSignature sig = fac.newXMLSignature(si, ki);
		DOMSignContext signContext = new DOMSignContext((Key) obj[1], token);
		signContext.putNamespacePrefix(javax.xml.crypto.dsig.XMLSignature.XMLNS, "ds");
        signContext.setProperty("org.jcp.xml.dsig.internal.dom.SignatureProvider", Security.getProvider("SC"));
        signContext.setProperty("org.jcp.xml.dsig.internal.dom.SecureRandom", random);
        signContext.setProperty("org.jcp.xml.dsig.internal.dom.DigestProvider", Security.getProvider("BC"));
		sig.sign(signContext);
		
		// Insert signature node in document
		Element sigE = (Element) XPathAPI.selectSingleNode(signContext.getParent(), "//ds:Signature");
		Node keyE = XPathAPI.selectSingleNode(sigE, "//ds:KeyInfo", sigE);
		token.appendChild(doc.createTextNode(XPathAPI.selectSingleNode(keyE, "//ds:X509Certificate", keyE).getFirstChild().getNodeValue()));
		keyE.removeChild(XPathAPI.selectSingleNode(keyE, "//ds:X509Data", keyE));
		NodeList chl = keyE.getChildNodes();
		
		for (int i = 0; i < chl.getLength(); i++) {
			keyE.removeChild(chl.item(i));
		}
		
		Node str = keyE.appendChild(doc.createElementNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse:SecurityTokenReference"));
		Element strRef = (Element)str.appendChild(doc.createElementNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse:Reference"));
		
		strRef.setAttribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3");
		strRef.setAttribute("URI", "#CertId");
		header.getSecurityHeader().appendChild(sigE);
	}	
	
	public static SOAPEnvelope getSOAPEnvelopeFromString(String message) throws Exception {
        
    	InputStream input = new ByteArrayInputStream(message.getBytes());
    	MessageFactory mf = MessageFactory.newInstance();
    	SOAPMessage msg = mf.createMessage(null, input);
    	return msg.getSOAPPart().getEnvelope();
/*    	
        Message msg = new Message(input);
        return msg.getSOAPEnvelope();
*/        
    }	
	
	public static SOAPMessage getSOAPMessageFromString(String message) throws SOAPException, IOException  
	{
        
    	InputStream input = new ByteArrayInputStream(message.getBytes());
    		MessageFactory msgFactory = MessageFactory.newInstance();    		
	    	try {
	    		SOAPMessage msg = msgFactory.createMessage(null, input);
	    		return msg;
			} finally {
	    		input.close();
	    	}

    }		
	
	public static String SOAPMessageToString(SOAPMessage msg) throws SOAPException 
	{
		ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
		try {
			msg.writeTo(byteArrayOS);
			return new String(byteArrayOS.toByteArray());
		} catch (IOException e) {
			throw new SOAPException(e);
		}

	}	
	
	public String getMessage() {

		String messageStr = null;
		
		try {
			// Create simple secured message
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage sm;
			
			String sxmlUnsigned = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				      "<SOAP-ENV:Envelope" + " xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
				      "<SOAP-ENV:Body>" +
				      "    <sayHello xmlns=\"http://jeffhanson.com/services/helloworld\">" +
				      "        <value xmlns=\"\">Hello world!</value>" +
				      "    </sayHello>" +
				      "</SOAP-ENV:Body>" +
				      "</SOAP-ENV:Envelope>";
			
			InputStream input = new ByteArrayInputStream(sxmlUnsigned.getBytes());
			try {
				 sm = mf.createMessage(null, input);
			} finally {
				input.close();
			}
			
			// Sign it
			constructSecuredMessage(sm);
			
			// Convert signed document to string for compatibility with basic interface
			messageStr = SOAPMessageToString(sm);
			System.out.println();
			System.out.println(messageStr);
			System.out.println();

		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WSSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return messageStr;
	}
}
