package org.admnkz.gostxml.dsig.internal.dom;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMCryptoContext;
import org.w3c.dom.Node;

public abstract class DOMStructure
  implements XMLStructure
{
  public final boolean isFeatureSupported(String feature)
  {
    if (feature == null) {
      throw new NullPointerException();
    }
    return false;
  }

  public abstract void marshal(Node paramNode, String paramString, DOMCryptoContext paramDOMCryptoContext)
    throws MarshalException;
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMStructure
 * JD-Core Version:    0.6.2
 */