package org.admnkz.gostxml.dsig.internal.dom;

import java.security.InvalidAlgorithmParameterException;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;

public final class DOMEnvelopedTransform extends ApacheTransform
{
  public void init(TransformParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if (params != null)
      throw new InvalidAlgorithmParameterException("params must be null");
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMEnvelopedTransform
 * JD-Core Version:    0.6.2
 */