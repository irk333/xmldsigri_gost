package org.admnkz.gostxml.dsig.internal.dom;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.AlgorithmParameterSpec;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLSignContext;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLValidateContext;
import javax.xml.crypto.dsig.spec.SignatureMethodParameterSpec;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.implementations.SignatureECDSA;
import org.w3c.dom.Element;
import org.admnkz.gostxml.dsig.internal.SignerOutputStream;

public abstract class DOMSignatureMethod extends AbstractDOMSignatureMethod
{
  private static Log log = LogFactory.getLog(DOMSignatureMethod.class);
  static final String RSA_SHA256 = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
  static final String RSA_SHA384 = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384";
  static final String RSA_SHA512 = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512";
  static final String ECDSA_SHA1 = "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha1";
  static final String ECDSA_SHA256 = "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256";
  static final String ECDSA_SHA384 = "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha384";
  static final String ECDSA_SHA512 = "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha512";
  static final String GOST3411_GOST3410 = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
  static final String GOST3411_GOST3410_URN = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102001-gostr3411";
  private SignatureMethodParameterSpec params;
  private Signature signature;

  DOMSignatureMethod(AlgorithmParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if ((params != null) && (!(params instanceof SignatureMethodParameterSpec)))
    {
      throw new InvalidAlgorithmParameterException("params must be of type SignatureMethodParameterSpec");
    }

    checkParams((SignatureMethodParameterSpec)params);
    this.params = ((SignatureMethodParameterSpec)params);
  }

  DOMSignatureMethod(Element smElem)
    throws MarshalException
  {
    Element paramsElem = DOMUtils.getFirstChildElement(smElem);
    if (paramsElem != null)
      this.params = unmarshalParams(paramsElem);
    try
    {
      checkParams(this.params);
    } catch (InvalidAlgorithmParameterException iape) {
      throw new MarshalException(iape);
    }
  }

  static SignatureMethod unmarshal(Element smElem) throws MarshalException {
    String alg = DOMUtils.getAttributeValue(smElem, "Algorithm");
    if (alg.equals("http://www.w3.org/2000/09/xmldsig#rsa-sha1"))
      return new SHA1withRSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"))
      return new SHA256withRSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#rsa-sha384"))
      return new SHA384withRSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#rsa-sha512"))
      return new SHA512withRSA(smElem);
    if (alg.equals("http://www.w3.org/2000/09/xmldsig#dsa-sha1"))
      return new SHA1withDSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha1"))
      return new SHA1withECDSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"))
      return new SHA256withECDSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha384"))
      return new SHA384withECDSA(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha512"))
      return new SHA512withECDSA(smElem);
    if (alg.equals("http://www.w3.org/2000/09/xmldsig#hmac-sha1"))
      return new DOMHMACSignatureMethod.SHA1(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#hmac-sha256"))
      return new DOMHMACSignatureMethod.SHA256(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#hmac-sha384"))
      return new DOMHMACSignatureMethod.SHA384(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#hmac-sha512"))
      return new DOMHMACSignatureMethod.SHA512(smElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411"))
      return new GOST3411withGOST3410(smElem);
    if (alg.equals("urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102001-gostr3411")) {
      return new GOST3411withGOST3410URN(smElem);
    }

    throw new MarshalException("unsupported SignatureMethod algorithm: " + alg);
  }

  public final AlgorithmParameterSpec getParameterSpec()
  {
    return this.params;
  }

  boolean verify(Key key, SignedInfo si, byte[] sig, XMLValidateContext context)
    throws InvalidKeyException, SignatureException, XMLSignatureException
  {
    if ((key == null) || (si == null) || (sig == null)) {
      throw new NullPointerException();
    }

    if (!(key instanceof PublicKey)) {
      throw new InvalidKeyException("key must be PublicKey");
    }
    if (this.signature == null) {
      try {
        Provider p = (Provider)context.getProperty("org.jcp.xml.dsig.internal.dom.SignatureProvider");

        this.signature = (p == null ? Signature.getInstance(getJCAAlgorithm()) : Signature.getInstance(getJCAAlgorithm(), p));
      }
      catch (NoSuchAlgorithmException nsae)
      {
        throw new XMLSignatureException(nsae);
      }
    }
    this.signature.initVerify((PublicKey)key);
    if (log.isDebugEnabled()) {
      log.debug("Signature provider:" + this.signature.getProvider());
      log.debug("verifying with key: " + key);
    }
    ((DOMSignedInfo)si).canonicalize(context, new SignerOutputStream(this.signature));
    try
    {
      AbstractDOMSignatureMethod.Type type = getAlgorithmType();
      if (type == AbstractDOMSignatureMethod.Type.DSA)
        return this.signature.verify(convertXMLDSIGtoASN1(sig));
      if (type == AbstractDOMSignatureMethod.Type.ECDSA) {
        return this.signature.verify(SignatureECDSA.convertXMLDSIGtoASN1(sig));
      }
      return this.signature.verify(sig);
    }
    catch (IOException ioe) {
      throw new XMLSignatureException(ioe);
    }
  }

  byte[] sign(Key key, SignedInfo si, XMLSignContext context)
    throws InvalidKeyException, XMLSignatureException
  {
    if ((key == null) || (si == null)) {
      throw new NullPointerException();
    }

    if (!(key instanceof PrivateKey)) {
      throw new InvalidKeyException("key must be PrivateKey");
    }
    if (this.signature == null) {
      try {
        Provider p = (Provider)context.getProperty("org.jcp.xml.dsig.internal.dom.SignatureProvider");

        this.signature = (p == null ? Signature.getInstance(getJCAAlgorithm()) : Signature.getInstance(getJCAAlgorithm(), p));
      }
      catch (NoSuchAlgorithmException nsae)
      {
        throw new XMLSignatureException(nsae);
      }
    }
    SecureRandom sr = (SecureRandom) context.getProperty("org.jcp.xml.dsig.internal.dom.SecureRandom");    
    if (sr == null)
    	this.signature.initSign((PrivateKey)key);
    else
    	this.signature.initSign((PrivateKey)key, sr);
    if (log.isDebugEnabled()) {
      log.debug("Signature provider:" + this.signature.getProvider());
      log.debug("Signing with key: " + key);
    }

    ((DOMSignedInfo)si).canonicalize(context, new SignerOutputStream(this.signature));
    try
    {
      AbstractDOMSignatureMethod.Type type = getAlgorithmType();
      if (type == AbstractDOMSignatureMethod.Type.DSA)
        return convertASN1toXMLDSIG(this.signature.sign());
      if (type == AbstractDOMSignatureMethod.Type.ECDSA) {
        return SignatureECDSA.convertASN1toXMLDSIG(this.signature.sign());
      }
      return this.signature.sign();
    }
    catch (SignatureException se) {
      throw new XMLSignatureException(se);
    } catch (IOException ioe) {
      throw new XMLSignatureException(ioe);
    }
  }

  private static byte[] convertASN1toXMLDSIG(byte[] asn1Bytes)
    throws IOException
  {
    byte rLength = asn1Bytes[3];

    int i = rLength;
    for (; (i > 0) && (asn1Bytes[(4 + rLength - i)] == 0); i--);
    byte sLength = asn1Bytes[(5 + rLength)];

    int j = sLength;
    while ((j > 0) && (asn1Bytes[(6 + rLength + sLength - j)] == 0)) j--;

    if ((asn1Bytes[0] != 48) || (asn1Bytes[1] != asn1Bytes.length - 2) || (asn1Bytes[2] != 2) || (i > 20) || (asn1Bytes[(4 + rLength)] != 2) || (j > 20))
    {
      throw new IOException("Invalid ASN.1 format of DSA signature");
    }
    byte[] xmldsigBytes = new byte[40];

    System.arraycopy(asn1Bytes, 4 + rLength - i, xmldsigBytes, 20 - i, i);
    System.arraycopy(asn1Bytes, 6 + rLength + sLength - j, xmldsigBytes, 40 - j, j);

    return xmldsigBytes;
  }

  private static byte[] convertXMLDSIGtoASN1(byte[] xmldsigBytes)
    throws IOException
  {
    if (xmldsigBytes.length != 40) {
      throw new IOException("Invalid XMLDSIG format of DSA signature");
    }

    int i = 20;
    for (; (i > 0) && (xmldsigBytes[(20 - i)] == 0); i--);
    int j = i;

    if (xmldsigBytes[(20 - i)] < 0) {
      j++;
    }

    int k = 20;
    for (; (k > 0) && (xmldsigBytes[(40 - k)] == 0); k--);
    int l = k;

    if (xmldsigBytes[(40 - k)] < 0) {
      l++;
    }

    byte[] asn1Bytes = new byte[6 + j + l];

    asn1Bytes[0] = 48;
    asn1Bytes[1] = ((byte)(4 + j + l));
    asn1Bytes[2] = 2;
    asn1Bytes[3] = ((byte)j);

    System.arraycopy(xmldsigBytes, 20 - i, asn1Bytes, 4 + j - i, i);

    asn1Bytes[(4 + j)] = 2;
    asn1Bytes[(5 + j)] = ((byte)l);

    System.arraycopy(xmldsigBytes, 40 - k, asn1Bytes, 6 + j + l - k, k);

    return asn1Bytes;
  }

  static final class GOST3411withGOST3410URN extends DOMSignatureMethod
  {
    GOST3411withGOST3410URN(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    GOST3411withGOST3410URN(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102001-gostr3411";
    }
    String getJCAAlgorithm() {
// TODO    	
    	return "GOST3411withECGOST3410";
//      return "GOST3411withGOST3410EL";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.GOST;
    }
  }

  static final class GOST3411withGOST3410 extends DOMSignatureMethod
  {
    GOST3411withGOST3410(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    GOST3411withGOST3410(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
    }
    String getJCAAlgorithm() {
    	// TODO    	
    	return "GOST3411withECGOST3410";
//      return "GOST3411withGOST3410EL";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.GOST;
    }
  }

  static final class SHA512withECDSA extends DOMSignatureMethod
  {
    SHA512withECDSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA512withECDSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha512";
    }
    String getJCAAlgorithm() {
      return "SHA512withECDSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.ECDSA;
    }
  }

  static final class SHA384withECDSA extends DOMSignatureMethod
  {
    SHA384withECDSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA384withECDSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha384";
    }
    String getJCAAlgorithm() {
      return "SHA384withECDSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.ECDSA;
    }
  }

  static final class SHA256withECDSA extends DOMSignatureMethod
  {
    SHA256withECDSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA256withECDSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256";
    }
    String getJCAAlgorithm() {
      return "SHA256withECDSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.ECDSA;
    }
  }

  static final class SHA1withECDSA extends DOMSignatureMethod
  {
    SHA1withECDSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA1withECDSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha1";
    }
    String getJCAAlgorithm() {
      return "SHA1withECDSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.ECDSA;
    }
  }

  static final class SHA1withDSA extends DOMSignatureMethod
  {
    SHA1withDSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA1withDSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
    }
    String getJCAAlgorithm() {
      return "SHA1withDSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.DSA;
    }
  }

  static final class SHA512withRSA extends DOMSignatureMethod
  {
    SHA512withRSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA512withRSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512";
    }
    String getJCAAlgorithm() {
      return "SHA512withRSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.RSA;
    }
  }

  static final class SHA384withRSA extends DOMSignatureMethod
  {
    SHA384withRSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA384withRSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384";
    }
    String getJCAAlgorithm() {
      return "SHA384withRSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.RSA;
    }
  }

  static final class SHA256withRSA extends DOMSignatureMethod
  {
    SHA256withRSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA256withRSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
    }
    String getJCAAlgorithm() {
      return "SHA256withRSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.RSA;
    }
  }

  static final class SHA1withRSA extends DOMSignatureMethod
  {
    SHA1withRSA(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA1withRSA(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
    }
    String getJCAAlgorithm() {
      return "SHA1withRSA";
    }
    AbstractDOMSignatureMethod.Type getAlgorithmType() {
      return AbstractDOMSignatureMethod.Type.RSA;
    }
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMSignatureMethod
 * JD-Core Version:    0.6.2
 */