package org.admnkz.gostxml.dsig.internal.dom;

import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.Provider;
import javax.xml.crypto.Data;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.TransformService;
import org.w3c.dom.Element;

public class DOMCanonicalizationMethod extends DOMTransform
  implements CanonicalizationMethod
{
  public DOMCanonicalizationMethod(TransformService spi)
    throws InvalidAlgorithmParameterException
  {
    super(spi);
  }

  public DOMCanonicalizationMethod(Element cmElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    super(cmElem, context, provider);
  }

  public Data canonicalize(Data data, XMLCryptoContext xc)
    throws TransformException
  {
    return transform(data, xc);
  }

  public Data canonicalize(Data data, XMLCryptoContext xc, OutputStream os)
    throws TransformException
  {
    return transform(data, xc, os);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof CanonicalizationMethod)) {
      return false;
    }
    CanonicalizationMethod ocm = (CanonicalizationMethod)o;

    return (getAlgorithm().equals(ocm.getAlgorithm())) && (DOMUtils.paramsEqual(getParameterSpec(), ocm.getParameterSpec()));
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMCanonicalizationMethod
 * JD-Core Version:    0.6.2
 */