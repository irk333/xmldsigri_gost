package org.admnkz.gostxml.dsig.internal.dom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.SignatureProperties;
import javax.xml.crypto.dsig.SignatureProperty;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class DOMSignatureProperties extends DOMStructure
  implements SignatureProperties
{
  private final String id;
  private final List<SignatureProperty> properties;

  public DOMSignatureProperties(List<? extends SignatureProperty> properties, String id)
  {
    if (properties == null)
      throw new NullPointerException("properties cannot be null");
    if (properties.isEmpty()) {
      throw new IllegalArgumentException("properties cannot be empty");
    }
    this.properties = Collections.unmodifiableList(new ArrayList(properties));

    int i = 0; for (int size = this.properties.size(); i < size; i++) {
      if (!(this.properties.get(i) instanceof SignatureProperty)) {
        throw new ClassCastException("properties[" + i + "] is not a valid type");
      }

    }

    this.id = id;
  }

  public DOMSignatureProperties(Element propsElem, XMLCryptoContext context)
    throws MarshalException
  {
    Attr attr = propsElem.getAttributeNodeNS(null, "Id");
    if (attr != null) {
      this.id = attr.getValue();
      propsElem.setIdAttributeNode(attr, true);
    } else {
      this.id = null;
    }

    NodeList nodes = propsElem.getChildNodes();
    int length = nodes.getLength();
    List properties = new ArrayList(length);

    for (int i = 0; i < length; i++) {
      Node child = nodes.item(i);
      if (child.getNodeType() == 1) {
        properties.add(new DOMSignatureProperty((Element)child, context));
      }
    }

    if (properties.isEmpty()) {
      throw new MarshalException("properties cannot be empty");
    }
    this.properties = Collections.unmodifiableList(properties);
  }

  public List getProperties()
  {
    return this.properties;
  }

  public String getId() {
    return this.id;
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);
    Element propsElem = DOMUtils.createElement(ownerDoc, "SignatureProperties", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMUtils.setAttributeID(propsElem, "Id", this.id);

    for (SignatureProperty property : this.properties) {
      ((DOMSignatureProperty)property).marshal(propsElem, dsPrefix, context);
    }

    parent.appendChild(propsElem);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof SignatureProperties)) {
      return false;
    }
    SignatureProperties osp = (SignatureProperties)o;

    boolean idsEqual = this.id == null ? false : osp.getId() == null ? true : this.id.equals(osp.getId());

    return (this.properties.equals(osp.getProperties())) && (idsEqual);
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMSignatureProperties
 * JD-Core Version:    0.6.2
 */