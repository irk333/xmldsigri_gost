package org.admnkz.gostxml.dsig.internal.dom;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.AccessController;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivilegedAction;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.xml.crypto.Data;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.NodeSetData;
import javax.xml.crypto.OctetStreamData;
import javax.xml.crypto.URIDereferencer;
import javax.xml.crypto.URIReferenceException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dom.DOMURIReference;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.TransformService;
import javax.xml.crypto.dsig.XMLSignContext;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLValidateContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.UnsyncBufferedOutputStream;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.admnkz.gostxml.dsig.internal.DigesterOutputStream;

public final class DOMReference extends DOMStructure
  implements Reference, DOMURIReference
{
  public static final int MAXIMUM_TRANSFORM_COUNT = 5;
  private static boolean useC14N11 = ((Boolean)AccessController.doPrivileged(new PrivilegedAction()
  {
    public Boolean run() {
      return Boolean.valueOf(Boolean.getBoolean("com.sun.org.apache.xml.internal.security.useC14N11"));
    }
  })).booleanValue();

  private static Log log = LogFactory.getLog(DOMReference.class);
  private final DigestMethod digestMethod;
  private final String id;
  private final List<Transform> transforms;
  private List<Transform> allTransforms;
  private final Data appliedTransformData;
  private Attr here;
  private final String uri;
  private final String type;
  private byte[] digestValue;
  private byte[] calcDigestValue;
  private Element refElem;
  private boolean digested = false;
  private boolean validated = false;
  private boolean validationStatus;
  private Data derefData;
  private InputStream dis;
  private MessageDigest md;
  private Provider provider;

  public DOMReference(String uri, String type, DigestMethod dm, List<? extends Transform> transforms, String id, Provider provider)
  {
    this(uri, type, dm, null, null, transforms, id, null, provider);
  }

  public DOMReference(String uri, String type, DigestMethod dm, List<? extends Transform> appliedTransforms, Data result, List<? extends Transform> transforms, String id, Provider provider)
  {
    this(uri, type, dm, appliedTransforms, result, transforms, id, null, provider);
  }

  public DOMReference(String uri, String type, DigestMethod dm, List<? extends Transform> appliedTransforms, Data result, List<? extends Transform> transforms, String id, byte[] digestValue, Provider provider)
  {
    if (dm == null) {
      throw new NullPointerException("DigestMethod must be non-null");
    }
    if (appliedTransforms == null) {
      this.allTransforms = new ArrayList();
    } else {
      this.allTransforms = new ArrayList(appliedTransforms);
      int i = 0; for (int size = this.allTransforms.size(); i < size; i++) {
        if (!(this.allTransforms.get(i) instanceof Transform)) {
          throw new ClassCastException("appliedTransforms[" + i + "] is not a valid type");
        }
      }
    }

    if (transforms == null) {
      this.transforms = Collections.emptyList();
    } else {
      this.transforms = new ArrayList(transforms);
      int i = 0; for (int size = this.transforms.size(); i < size; i++) {
        if (!(this.transforms.get(i) instanceof Transform)) {
          throw new ClassCastException("transforms[" + i + "] is not a valid type");
        }
      }

      this.allTransforms.addAll(this.transforms);
    }
    this.digestMethod = dm;
    this.uri = uri;
    if ((uri != null) && (!uri.equals(""))) {
      try {
        new URI(uri);
      } catch (URISyntaxException e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }
    this.type = type;
    this.id = id;
    if (digestValue != null) {
      this.digestValue = ((byte[])digestValue.clone());
      this.digested = true;
    }
    this.appliedTransformData = result;
    this.provider = provider;
  }

  public DOMReference(Element refElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    Boolean secureValidation = (Boolean)context.getProperty("org.apache.jcp.xml.dsig.secureValidation");

    boolean secVal = false;
    if ((secureValidation != null) && (secureValidation.booleanValue())) {
      secVal = true;
    }

    Element nextSibling = DOMUtils.getFirstChildElement(refElem);
    List transforms = new ArrayList(5);
    if (Utils.extractLocalName(nextSibling.getTagName()).equals("Transforms")) {
      Element transformElem = DOMUtils.getFirstChildElement(nextSibling);

      int transformCount = 0;
      while (transformElem != null) {
        transforms.add(new DOMTransform(transformElem, context, provider));

        transformElem = DOMUtils.getNextSiblingElement(transformElem);

        transformCount++;
        if ((secVal) && (transformCount > 5)) {
          String error = "A maxiumum of 5 transforms per Reference are allowed with secure validation";

          throw new MarshalException(error);
        }
      }
      nextSibling = DOMUtils.getNextSiblingElement(nextSibling);
    }

    Element dmElem = nextSibling;
    this.digestMethod = DOMDigestMethod.unmarshal(dmElem);
    if ((secVal) && ("http://www.w3.org/2001/04/xmldsig-more#md5".equals(this.digestMethod))) {
      throw new MarshalException("It is forbidden to use algorithm " + this.digestMethod + " when secure validation is enabled");
    }

    try
    {
      Element dvElem = DOMUtils.getNextSiblingElement(dmElem);
      this.digestValue = Base64.decode(dvElem);
    } catch (Base64DecodingException bde) {
      throw new MarshalException(bde);
    }

    this.uri = DOMUtils.getAttributeValue(refElem, "URI");

    Attr attr = refElem.getAttributeNodeNS(null, "Id");
    if (attr != null) {
      this.id = attr.getValue();
      refElem.setIdAttributeNode(attr, true);
    } else {
      this.id = null;
    }

    this.type = DOMUtils.getAttributeValue(refElem, "Type");
    this.here = refElem.getAttributeNodeNS(null, "URI");
    this.refElem = refElem;
    this.transforms = transforms;
    this.allTransforms = transforms;
    this.appliedTransformData = null;
    this.provider = provider;
  }

  public DigestMethod getDigestMethod() {
    return this.digestMethod;
  }

  public String getId() {
    return this.id;
  }

  public String getURI() {
    return this.uri;
  }

  public String getType() {
    return this.type;
  }

  public List getTransforms() {
    return Collections.unmodifiableList(this.allTransforms);
  }

  public byte[] getDigestValue() {
    return this.digestValue == null ? null : (byte[])this.digestValue.clone();
  }

  public byte[] getCalculatedDigestValue() {
    return this.calcDigestValue == null ? null : (byte[])this.calcDigestValue.clone();
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    if (log.isDebugEnabled()) {
      log.debug("Marshalling Reference");
    }
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);

    this.refElem = DOMUtils.createElement(ownerDoc, "Reference", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMUtils.setAttributeID(this.refElem, "Id", this.id);
    DOMUtils.setAttribute(this.refElem, "URI", this.uri);
    DOMUtils.setAttribute(this.refElem, "Type", this.type);
    Element transformsElem;
    if (!this.allTransforms.isEmpty()) {
      transformsElem = DOMUtils.createElement(ownerDoc, "Transforms", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      this.refElem.appendChild(transformsElem);
      for (Transform transform : this.allTransforms) {
        ((DOMStructure)transform).marshal(transformsElem, dsPrefix, context);
      }

    }

    ((DOMDigestMethod)this.digestMethod).marshal(this.refElem, dsPrefix, context);

    if (log.isDebugEnabled()) {
      log.debug("Adding digestValueElem");
    }
    Element digestValueElem = DOMUtils.createElement(ownerDoc, "DigestValue", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    if (this.digestValue != null) {
      digestValueElem.appendChild(ownerDoc.createTextNode(Base64.encode(this.digestValue)));
    }

    this.refElem.appendChild(digestValueElem);

    parent.appendChild(this.refElem);
    this.here = this.refElem.getAttributeNodeNS(null, "URI");
  }

  public void digest(XMLSignContext signContext)
    throws XMLSignatureException
  {
    Data data = null;
    if (this.appliedTransformData == null)
      data = dereference(signContext);
    else {
      data = this.appliedTransformData;
    }
    this.digestValue = transform(data, signContext);

    String encodedDV = Base64.encode(this.digestValue);
    if (log.isDebugEnabled()) {
      log.debug("Reference object uri = " + this.uri);
    }
    Element digestElem = DOMUtils.getLastChildElement(this.refElem);
    if (digestElem == null) {
      throw new XMLSignatureException("DigestValue element expected");
    }
    DOMUtils.removeAllChildren(digestElem);
    digestElem.appendChild(this.refElem.getOwnerDocument().createTextNode(encodedDV));

    this.digested = true;
    if (log.isDebugEnabled())
      log.debug("Reference digesting completed");
  }

  public boolean validate(XMLValidateContext validateContext)
    throws XMLSignatureException
  {
    if (validateContext == null) {
      throw new NullPointerException("validateContext cannot be null");
    }
    if (this.validated) {
      return this.validationStatus;
    }
    Data data = dereference(validateContext);
    this.calcDigestValue = transform(data, validateContext);

    if (log.isDebugEnabled()) {
      log.debug("Expected digest: " + Base64.encode(this.digestValue));
      log.debug("Actual digest: " + Base64.encode(this.calcDigestValue));
    }

    this.validationStatus = Arrays.equals(this.digestValue, this.calcDigestValue);
    this.validated = true;
    return this.validationStatus;
  }

  public Data getDereferencedData() {
    return this.derefData;
  }

  public InputStream getDigestInputStream() {
    return this.dis;
  }

  private Data dereference(XMLCryptoContext context)
    throws XMLSignatureException
  {
    Data data = null;

    URIDereferencer deref = context.getURIDereferencer();
    if (deref == null)
      deref = DOMURIDereferencer.INSTANCE;
    try
    {
      data = deref.dereference(this, context);
      if (log.isDebugEnabled()) {
        log.debug("URIDereferencer class name: " + deref.getClass().getName());
        log.debug("Data class name: " + data.getClass().getName());
      }
    } catch (URIReferenceException ure) {
      throw new XMLSignatureException(ure);
    }

    return data;
  }

  private byte[] transform(Data dereferencedData, XMLCryptoContext context)
    throws XMLSignatureException
  {
    if (this.md == null) {
      try {
    	  Provider digProv = (Provider) context.getProperty("org.jcp.xml.dsig.internal.dom.DigestProvider");
    	  if (digProv == null)
    		  this.md = MessageDigest.getInstance(((DOMDigestMethod)this.digestMethod).getMessageDigestAlgorithm());
    	  else
    		  this.md = MessageDigest.getInstance(((DOMDigestMethod)this.digestMethod).getMessageDigestAlgorithm(), digProv);
      }
      catch (NoSuchAlgorithmException nsae) {
        throw new XMLSignatureException(nsae);
      } 
    }
    this.md.reset();

    Boolean cache = (Boolean)context.getProperty("javax.xml.crypto.dsig.cacheReference");
    DigesterOutputStream dos;
    if ((cache != null) && (cache.booleanValue() == true)) {
      this.derefData = copyDerefData(dereferencedData);
      dos = new DigesterOutputStream(this.md, true);
    } else {
      dos = new DigesterOutputStream(this.md);
    }
    OutputStream os = new UnsyncBufferedOutputStream(dos);
    Data data = dereferencedData;
    int i = 0; for (int size = this.transforms.size(); i < size; i++) {
      DOMTransform transform = (DOMTransform)this.transforms.get(i);
      try {
        if (i < size - 1)
          data = transform.transform(data, context);
        else
          data = transform.transform(data, context, os);
      }
      catch (TransformException te) {
        throw new XMLSignatureException(te);
      }
    }
    try
    {
      if (data != null)
      {
        boolean c14n11 = useC14N11;
        String c14nalg = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
        if ((context instanceof XMLSignContext))
          if (!c14n11) {
            Boolean prop = (Boolean)context.getProperty("org.apache.xml.security.useC14N11");

            c14n11 = (prop != null) && (prop.booleanValue() == true);
            if (c14n11)
              c14nalg = "http://www.w3.org/2006/12/xml-c14n11";
          }
          else {
            c14nalg = "http://www.w3.org/2006/12/xml-c14n11";
          }
        XMLSignatureInput xi;
        if ((data instanceof ApacheData)) {
          xi = ((ApacheData)data).getXMLSignatureInput();
        }
        else
        {
          if ((data instanceof OctetStreamData)) {
            xi = new XMLSignatureInput(((OctetStreamData)data).getOctetStream());
          }
          else
          {
            if ((data instanceof NodeSetData)) {
              TransformService spi = null;
              if (this.provider == null)
                spi = TransformService.getInstance(c14nalg, "DOM");
              else {
                try {
                  spi = TransformService.getInstance(c14nalg, "DOM", this.provider);
                } catch (NoSuchAlgorithmException nsae) {
                  spi = TransformService.getInstance(c14nalg, "DOM");
                }
              }
              data = spi.transform(data, context);
              xi = new XMLSignatureInput(((OctetStreamData)data).getOctetStream());
            }
            else {
              throw new XMLSignatureException("unrecognized Data type");
            }
          }
        }
        if (((context instanceof XMLSignContext)) && (c14n11) && (!xi.isOctetStream()) && (!xi.isOutputStreamSet()))
        {
          TransformService spi = null;
          if (this.provider == null)
            spi = TransformService.getInstance(c14nalg, "DOM");
          else {
            try {
              spi = TransformService.getInstance(c14nalg, "DOM", this.provider);
            } catch (NoSuchAlgorithmException nsae) {
              spi = TransformService.getInstance(c14nalg, "DOM");
            }
          }

          DOMTransform t = new DOMTransform(spi);
          Element transformsElem = null;
          String dsPrefix = DOMUtils.getSignaturePrefix(context);
          if (this.allTransforms.isEmpty()) {
            transformsElem = DOMUtils.createElement(this.refElem.getOwnerDocument(), "Transforms", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

            this.refElem.insertBefore(transformsElem, DOMUtils.getFirstChildElement(this.refElem));
          }
          else {
            transformsElem = DOMUtils.getFirstChildElement(this.refElem);
          }
          t.marshal(transformsElem, dsPrefix, (DOMCryptoContext)context);

          this.allTransforms.add(t);
          xi.updateOutputStream(os, true);
        } else {
          xi.updateOutputStream(os);
        }
      }
      os.flush();
      if ((cache != null) && (cache.booleanValue() == true)) {
        this.dis = dos.getInputStream();
      }
      return dos.getDigestValue();
    } catch (NoSuchAlgorithmException e) {
      throw new XMLSignatureException(e);
    } catch (TransformException e) {
      throw new XMLSignatureException(e);
    } catch (MarshalException e) {
      throw new XMLSignatureException(e);
    } catch (IOException e) {
      throw new XMLSignatureException(e);
    } catch (CanonicalizationException e) {
      throw new XMLSignatureException(e);
    }
  }

  public Node getHere() {
    return this.here;
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof DOMURIReference)) {
      return false;
    }
    Reference oref = (Reference)o;

    boolean idsEqual = this.id == null ? false : oref.getId() == null ? true : this.id.equals(oref.getId());

    boolean urisEqual = this.uri == null ? false : oref.getURI() == null ? true : this.uri.equals(oref.getURI());

    boolean typesEqual = this.type == null ? false : oref.getType() == null ? true : this.type.equals(oref.getType());

    boolean digestValuesEqual = Arrays.equals(this.digestValue, oref.getDigestValue());

    return (this.digestMethod.equals(oref.getDigestMethod())) && (idsEqual) && (urisEqual) && (typesEqual) && (this.allTransforms.equals(oref.getTransforms())) && (digestValuesEqual);
  }

  boolean isDigested()
  {
    return this.digested;
  }

  private static Data copyDerefData(Data dereferencedData) {
    if ((dereferencedData instanceof ApacheData))
    {
      ApacheData ad = (ApacheData)dereferencedData;
      XMLSignatureInput xsi = ad.getXMLSignatureInput();
      if (xsi.isNodeSet())
        try {
          final Set s = xsi.getNodeSet();
          return new NodeSetData() {
            public Iterator iterator() { return s.iterator(); }
          };
        }
        catch (Exception e) {
          log.warn("cannot cache dereferenced data: " + e);
          return null;
        }
      if (xsi.isElement()) {
        return new DOMSubTreeData(xsi.getSubNode(), xsi.isExcludeComments());
      }
      if ((xsi.isOctetStream()) || (xsi.isByteArray())) {
        try {
          return new OctetStreamData(xsi.getOctetStream(), xsi.getSourceURI(), xsi.getMIMEType());
        }
        catch (IOException ioe)
        {
          log.warn("cannot cache dereferenced data: " + ioe);
          return null;
        }
      }
    }
    return dereferencedData;
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMReference
 * JD-Core Version:    0.6.2
 */