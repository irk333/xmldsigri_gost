package org.admnkz.gostxml.dsig.internal.dom;

import java.security.InvalidAlgorithmParameterException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public final class DOMXSLTTransform extends ApacheTransform
{
  public void init(TransformParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if (params == null) {
      throw new InvalidAlgorithmParameterException("params are required");
    }
    if (!(params instanceof XSLTTransformParameterSpec)) {
      throw new InvalidAlgorithmParameterException("unrecognized params");
    }
    this.params = params;
  }

  public void init(XMLStructure parent, XMLCryptoContext context)
    throws InvalidAlgorithmParameterException
  {
    super.init(parent, context);
    unmarshalParams(DOMUtils.getFirstChildElement(this.transformElem));
  }

  private void unmarshalParams(Element sheet) {
    this.params = new XSLTTransformParameterSpec(new DOMStructure(sheet));
  }

  public void marshalParams(XMLStructure parent, XMLCryptoContext context)
    throws MarshalException
  {
    super.marshalParams(parent, context);
    XSLTTransformParameterSpec xp = (XSLTTransformParameterSpec)getParameterSpec();

    Node xsltElem = ((DOMStructure)xp.getStylesheet()).getNode();

    DOMUtils.appendChild(this.transformElem, xsltElem);
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMXSLTTransform
 * JD-Core Version:    0.6.2
 */