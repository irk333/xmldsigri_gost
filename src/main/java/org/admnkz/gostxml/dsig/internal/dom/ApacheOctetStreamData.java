package org.admnkz.gostxml.dsig.internal.dom;

import java.io.IOException;
import javax.xml.crypto.OctetStreamData;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.signature.XMLSignatureInput;

public class ApacheOctetStreamData extends OctetStreamData
  implements ApacheData
{
  private XMLSignatureInput xi;

  public ApacheOctetStreamData(XMLSignatureInput xi)
    throws CanonicalizationException, IOException
  {
    super(xi.getOctetStream(), xi.getSourceURI(), xi.getMIMEType());
    this.xi = xi;
  }

  public XMLSignatureInput getXMLSignatureInput() {
    return this.xi;
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.ApacheOctetStreamData
 * JD-Core Version:    0.6.2
 */