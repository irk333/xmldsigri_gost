package org.admnkz.gostxml.dsig.internal.dom;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.Data;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.URIDereferencer;
import javax.xml.crypto.URIReferenceException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dom.DOMURIReference;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public final class DOMRetrievalMethod extends DOMStructure
  implements RetrievalMethod, DOMURIReference
{
  private final List<Transform> transforms;
  private String uri;
  private String type;
  private Attr here;

  public DOMRetrievalMethod(String uri, String type, List<? extends Transform> transforms)
  {
    if (uri == null) {
      throw new NullPointerException("uri cannot be null");
    }
    if ((transforms == null) || (transforms.isEmpty())) {
      this.transforms = Collections.emptyList();
    } else {
      this.transforms = Collections.unmodifiableList(new ArrayList(transforms));

      int i = 0; for (int size = this.transforms.size(); i < size; i++) {
        if (!(this.transforms.get(i) instanceof Transform)) {
          throw new ClassCastException("transforms[" + i + "] is not a valid type");
        }
      }
    }

    this.uri = uri;
    if ((uri != null) && (!uri.equals(""))) {
      try {
        new URI(uri);
      } catch (URISyntaxException e) {
        throw new IllegalArgumentException(e.getMessage());
      }
    }

    this.type = type;
  }

  public DOMRetrievalMethod(Element rmElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    this.uri = DOMUtils.getAttributeValue(rmElem, "URI");
    this.type = DOMUtils.getAttributeValue(rmElem, "Type");

    this.here = rmElem.getAttributeNodeNS(null, "URI");

    Boolean secureValidation = (Boolean)context.getProperty("org.apache.jcp.xml.dsig.secureValidation");

    boolean secVal = false;
    if ((secureValidation != null) && (secureValidation.booleanValue())) {
      secVal = true;
    }

    List transforms = new ArrayList();
    Element transformsElem = DOMUtils.getFirstChildElement(rmElem);

    int transformCount = 0;
    if (transformsElem != null) {
      Element transformElem = DOMUtils.getFirstChildElement(transformsElem);

      while (transformElem != null) {
        transforms.add(new DOMTransform(transformElem, context, provider));

        transformElem = DOMUtils.getNextSiblingElement(transformElem);

        transformCount++;
        if ((secVal) && (transformCount > 5)) {
          String error = "A maxiumum of 5 transforms per Reference are allowed with secure validation";

          throw new MarshalException(error);
        }
      }
    }
    if (transforms.isEmpty())
      this.transforms = Collections.emptyList();
    else
      this.transforms = Collections.unmodifiableList(transforms);
  }

  public String getURI()
  {
    return this.uri;
  }

  public String getType() {
    return this.type;
  }

  public List getTransforms() {
    return this.transforms;
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);
    Element rmElem = DOMUtils.createElement(ownerDoc, "RetrievalMethod", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMUtils.setAttribute(rmElem, "URI", this.uri);
    DOMUtils.setAttribute(rmElem, "Type", this.type);
    Element transformsElem;
    if (!this.transforms.isEmpty()) {
      transformsElem = DOMUtils.createElement(ownerDoc, "Transforms", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      rmElem.appendChild(transformsElem);
      for (Transform transform : this.transforms) {
        ((DOMTransform)transform).marshal(transformsElem, dsPrefix, context);
      }

    }

    parent.appendChild(rmElem);

    this.here = rmElem.getAttributeNodeNS(null, "URI");
  }

  public Node getHere() {
    return this.here;
  }

  public Data dereference(XMLCryptoContext context)
    throws URIReferenceException
  {
    if (context == null) {
      throw new NullPointerException("context cannot be null");
    }

    URIDereferencer deref = context.getURIDereferencer();
    if (deref == null) {
      deref = DOMURIDereferencer.INSTANCE;
    }

    Data data = deref.dereference(this, context);
    try
    {
      for (Transform transform : this.transforms)
        data = ((DOMTransform)transform).transform(data, context);
    }
    catch (Exception e) {
      throw new URIReferenceException(e);
    }
    return data;
  }

  public XMLStructure dereferenceAsXMLStructure(XMLCryptoContext context) throws URIReferenceException
  {
    try
    {
      ApacheData data = (ApacheData)dereference(context);
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      dbf.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", Boolean.TRUE.booleanValue());
      DocumentBuilder db = dbf.newDocumentBuilder();
      Document doc = db.parse(new ByteArrayInputStream(data.getXMLSignatureInput().getBytes()));

      Element kiElem = doc.getDocumentElement();
      if (Utils.extractLocalName(kiElem.getTagName()).equals("X509Data")) {
        return new DOMX509Data(kiElem);
      }
      return null;
    }
    catch (Exception e) {
      throw new URIReferenceException(e);
    }
  }

  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof DOMURIReference)) {
      return false;
    }
    RetrievalMethod orm = (RetrievalMethod)obj;

    boolean typesEqual = this.type == null ? false : orm.getType() == null ? true : this.type.equals(orm.getType());

    return (this.uri.equals(orm.getURI())) && (this.transforms.equals(orm.getTransforms())) && (typesEqual);
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMRetrievalMethod
 * JD-Core Version:    0.6.2
 */