package org.admnkz.gostxml.dsig.internal.dom;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.SignatureException;
import java.security.spec.AlgorithmParameterSpec;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLSignContext;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLValidateContext;
import javax.xml.crypto.dsig.spec.SignatureMethodParameterSpec;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

abstract class AbstractDOMSignatureMethod extends DOMStructure
  implements SignatureMethod
{
  abstract boolean verify(Key paramKey, SignedInfo paramSignedInfo, byte[] paramArrayOfByte, XMLValidateContext paramXMLValidateContext)
    throws InvalidKeyException, SignatureException, XMLSignatureException;

  abstract byte[] sign(Key paramKey, SignedInfo paramSignedInfo, XMLSignContext paramXMLSignContext)
    throws InvalidKeyException, XMLSignatureException;

  abstract String getJCAAlgorithm();

  abstract Type getAlgorithmType();

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);

    Element smElem = DOMUtils.createElement(ownerDoc, "SignatureMethod", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMUtils.setAttribute(smElem, "Algorithm", getAlgorithm());

    if (getParameterSpec() != null) {
      marshalParams(smElem, dsPrefix);
    }

    parent.appendChild(smElem);
  }

  void marshalParams(Element parent, String paramsPrefix)
    throws MarshalException
  {
    throw new MarshalException("no parameters should be specified for the " + getAlgorithm() + " SignatureMethod algorithm");
  }

  SignatureMethodParameterSpec unmarshalParams(Element paramsElem)
    throws MarshalException
  {
    throw new MarshalException("no parameters should be specified for the " + getAlgorithm() + " SignatureMethod algorithm");
  }

  void checkParams(SignatureMethodParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if (params != null)
      throw new InvalidAlgorithmParameterException("no parameters should be specified for the " + getAlgorithm() + " SignatureMethod algorithm");
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof SignatureMethod)) {
      return false;
    }
    SignatureMethod osm = (SignatureMethod)o;

    return (getAlgorithm().equals(osm.getAlgorithm())) && (paramsEqual(osm.getParameterSpec()));
  }

  boolean paramsEqual(AlgorithmParameterSpec spec)
  {
    return getParameterSpec() == spec;
  }

  static enum Type
  {
    DSA, RSA, ECDSA, HMAC, GOST;
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.AbstractDOMSignatureMethod
 * JD-Core Version:    0.6.2
 */