package org.admnkz.gostxml.dsig.internal.dom;

import javax.xml.crypto.Data;
import org.apache.xml.security.signature.XMLSignatureInput;

public abstract interface ApacheData extends Data
{
  public abstract XMLSignatureInput getXMLSignatureInput();
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.ApacheData
 * JD-Core Version:    0.6.2
 */