package org.admnkz.gostxml.dsig.internal.dom;

import java.security.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class DOMKeyInfo extends DOMStructure
  implements KeyInfo
{
  private final String id;
  private final List<XMLStructure> keyInfoTypes;

  public DOMKeyInfo(List<? extends XMLStructure> content, String id)
  {
    if (content == null) {
      throw new NullPointerException("content cannot be null");
    }
    this.keyInfoTypes = Collections.unmodifiableList(new ArrayList(content));

    if (this.keyInfoTypes.isEmpty()) {
      throw new IllegalArgumentException("content cannot be empty");
    }
    int i = 0; for (int size = this.keyInfoTypes.size(); i < size; i++) {
      if (!(this.keyInfoTypes.get(i) instanceof XMLStructure)) {
        throw new ClassCastException("content[" + i + "] is not a valid KeyInfo type");
      }
    }

    this.id = id;
  }

  public DOMKeyInfo(Element kiElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    Attr attr = kiElem.getAttributeNodeNS(null, "Id");
    if (attr != null) {
      this.id = attr.getValue();
      kiElem.setIdAttributeNode(attr, true);
    } else {
      this.id = null;
    }

    NodeList nl = kiElem.getChildNodes();
    int length = nl.getLength();
    if (length < 1) {
      throw new MarshalException("KeyInfo must contain at least one type");
    }

    List content = new ArrayList(length);
    for (int i = 0; i < length; i++) {
      Node child = nl.item(i);

      if (child.getNodeType() == 1)
      {
        Element childElem = (Element)child;
        String localName = Utils.extractLocalName( childElem.getTagName() );
        if (localName.equals("X509Data"))
          content.add(new DOMX509Data(childElem));
        else if (localName.equals("KeyName"))
          content.add(new DOMKeyName(childElem));
        else if (localName.equals("KeyValue"))
          content.add(DOMKeyValue.unmarshal(childElem));
        else if (localName.equals("RetrievalMethod")) {
          content.add(new DOMRetrievalMethod(childElem, context, provider));
        }
        else if (localName.equals("PGPData"))
          content.add(new DOMPGPData(childElem));
        else
          content.add(new javax.xml.crypto.dom.DOMStructure(childElem));
      }
    }
    this.keyInfoTypes = Collections.unmodifiableList(content);
  }

  public String getId() {
    return this.id;
  }

  public List getContent() {
    return this.keyInfoTypes;
  }

  public void marshal(XMLStructure parent, XMLCryptoContext context)
    throws MarshalException
  {
    if (parent == null) {
      throw new NullPointerException("parent is null");
    }

    Node pNode = ((javax.xml.crypto.dom.DOMStructure)parent).getNode();
    String dsPrefix = DOMUtils.getSignaturePrefix(context);
    Element kiElem = DOMUtils.createElement(DOMUtils.getOwnerDocument(pNode), "KeyInfo", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    if ((dsPrefix == null) || (dsPrefix.length() == 0)) {
      kiElem.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", "http://www.w3.org/2000/09/xmldsig#");
    }
    else {
      kiElem.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + dsPrefix, "http://www.w3.org/2000/09/xmldsig#");
    }

    marshal(pNode, kiElem, null, dsPrefix, (DOMCryptoContext)context);
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    marshal(parent, null, dsPrefix, context);
  }

  public void marshal(Node parent, Node nextSibling, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);
    Element kiElem = DOMUtils.createElement(ownerDoc, "KeyInfo", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    marshal(parent, kiElem, nextSibling, dsPrefix, context);
  }

  private void marshal(Node parent, Element kiElem, Node nextSibling, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    for (XMLStructure kiType : this.keyInfoTypes) {
      if ((kiType instanceof DOMStructure))
        ((DOMStructure)kiType).marshal(kiElem, dsPrefix, context);
      else {
        DOMUtils.appendChild(kiElem, ((javax.xml.crypto.dom.DOMStructure)kiType).getNode());
      }

    }

    DOMUtils.setAttributeID(kiElem, "Id", this.id);

    parent.insertBefore(kiElem, nextSibling);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof KeyInfo)) {
      return false;
    }
    KeyInfo oki = (KeyInfo)o;

    boolean idsEqual = this.id == null ? false : oki.getId() == null ? true : this.id.equals(oki.getId());

    return (this.keyInfoTypes.equals(oki.getContent())) && (idsEqual);
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMKeyInfo
 * JD-Core Version:    0.6.2
 */