package org.admnkz.gostxml.dsig.internal.dom;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.AccessController;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.PublicKey;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.EllipticCurve;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class DOMKeyValue extends DOMStructure
  implements KeyValue
{
  private static final String XMLDSIG_11_XMLNS = "http://www.w3.org/2009/xmldsig11#";
  private final PublicKey publicKey;

  public DOMKeyValue(PublicKey key)
    throws KeyException
  {
    if (key == null) {
      throw new NullPointerException("key cannot be null");
    }
    this.publicKey = key;
  }

  public DOMKeyValue(Element kvtElem)
    throws MarshalException
  {
    this.publicKey = unmarshalKeyValue(kvtElem);
  }

  static KeyValue unmarshal(Element kvElem) throws MarshalException {
    Element kvtElem = DOMUtils.getFirstChildElement(kvElem);
    if (Utils.extractLocalName( kvtElem.getTagName()).equals("DSAKeyValue"))
      return new DSA(kvtElem);
    if (Utils.extractLocalName(kvtElem.getTagName()).equals("RSAKeyValue"))
      return new RSA(kvtElem);
    if (Utils.extractLocalName(kvtElem.getTagName()).equals("ECKeyValue"))
      return new EC(kvtElem);
    if (Utils.extractLocalName(kvtElem.getTagName()).equals("GOSTKeyValue")) {
      return new GOST3410(kvtElem);
    }

    return new Unknown(kvtElem);
  }

  public PublicKey getPublicKey() throws KeyException
  {
    if (this.publicKey == null) {
      throw new KeyException("can't convert KeyValue to PublicKey");
    }
    return this.publicKey;
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);

    Element kvElem = DOMUtils.createElement(ownerDoc, "KeyValue", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    marshalPublicKey(kvElem, ownerDoc, dsPrefix, context);

    parent.appendChild(kvElem);
  }

  abstract void marshalPublicKey(Node paramNode, Document paramDocument, String paramString, DOMCryptoContext paramDOMCryptoContext) throws MarshalException;

  abstract PublicKey unmarshalKeyValue(Element paramElement) throws MarshalException;

  private static PublicKey generatePublicKey(KeyFactory kf, KeySpec keyspec)
  {
    try
    {
      return kf.generatePublic(keyspec);
    } catch (InvalidKeySpecException e) {
    }
    return null;
  }

  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof KeyValue))
      return false;
    try
    {
      KeyValue kv = (KeyValue)obj;
      if (this.publicKey == null) {
        if (kv.getPublicKey() != null)
          return false;
      }
      else if (!this.publicKey.equals(kv.getPublicKey()))
        return false;
    }
    catch (KeyException ke)
    {
      return false;
    }

    return true;
  }

  static final class Unknown extends DOMKeyValue
  {
    private javax.xml.crypto.dom.DOMStructure externalPublicKey;

    Unknown(Element elem)
      throws MarshalException
    {
      super(elem);
    }
    PublicKey unmarshalKeyValue(Element kvElem) throws MarshalException {
      this.externalPublicKey = new javax.xml.crypto.dom.DOMStructure(kvElem);
      return null;
    }

    void marshalPublicKey(Node parent, Document doc, String dsPrefix, DOMCryptoContext context)
      throws MarshalException
    {
      parent.appendChild(this.externalPublicKey.getNode());
    }
  }

  static final class EC extends DOMKeyValue
  {
    private byte[] ecPublicKey;
    private KeyFactory eckf;
    private ECParameterSpec ecParams;
    private Method encodePoint;
    private Method decodePoint;
    private Method getCurveName;
    private Method getECParameterSpec;

    EC(PublicKey key)
      throws KeyException
    {
      super(key);
      ECPublicKey ecKey = (ECPublicKey)key;
      ECPoint ecPoint = ecKey.getW();
      this.ecParams = ecKey.getParams();
      try {
        AccessController.doPrivileged(new PrivilegedExceptionAction()
        {
          public Void run()
            throws ClassNotFoundException, NoSuchMethodException
          {
            DOMKeyValue.EC.this.getMethods();
            return null;
          }
        });
      }
      catch (PrivilegedActionException pae) {
        throw new KeyException("ECKeyValue not supported", pae.getException());
      }

      Object[] args = { ecPoint, this.ecParams.getCurve() };
      try {
        this.ecPublicKey = ((byte[])this.encodePoint.invoke(null, args));
      } catch (IllegalAccessException iae) {
        throw new KeyException(iae);
      } catch (InvocationTargetException ite) {
        throw new KeyException(ite);
      }
    }

    EC(Element dmElem) throws MarshalException {
      super(dmElem);
    }

    void getMethods() throws ClassNotFoundException, NoSuchMethodException {
      Class c = Class.forName("sun.security.ec.ECParameters");
      Class[] params = { ECPoint.class, EllipticCurve.class };
      this.encodePoint = c.getMethod("encodePoint", params);
      params = new Class[] { ECParameterSpec.class };
      this.getCurveName = c.getMethod("getCurveName", params);
      params = new Class[] { ECPoint.class, EllipticCurve.class };
      this.decodePoint = c.getMethod("decodePoint", params);
      c = Class.forName("sun.security.ec.NamedCurve");
      params = new Class[] { String.class };
      this.getECParameterSpec = c.getMethod("getECParameterSpec", params);
    }

    void marshalPublicKey(Node parent, Document doc, String dsPrefix, DOMCryptoContext context)
      throws MarshalException
    {
      String prefix = DOMUtils.getNSPrefix(context, "http://www.w3.org/2009/xmldsig11#");
      Element ecKeyValueElem = DOMUtils.createElement(doc, "ECKeyValue", "http://www.w3.org/2009/xmldsig11#", prefix);

      Element namedCurveElem = DOMUtils.createElement(doc, "NamedCurve", "http://www.w3.org/2009/xmldsig11#", prefix);

      Element publicKeyElem = DOMUtils.createElement(doc, "PublicKey", "http://www.w3.org/2009/xmldsig11#", prefix);

      Object[] args = { this.ecParams };
      try {
        String oid = (String)this.getCurveName.invoke(null, args);
        DOMUtils.setAttribute(namedCurveElem, "URI", "urn:oid:" + oid);
      } catch (IllegalAccessException iae) {
        throw new MarshalException(iae);
      } catch (InvocationTargetException ite) {
        throw new MarshalException(ite);
      }
      String qname = "xmlns:" + prefix;

      namedCurveElem.setAttributeNS("http://www.w3.org/2000/xmlns/", qname, "http://www.w3.org/2009/xmldsig11#");

      ecKeyValueElem.appendChild(namedCurveElem);
      String encoded = Base64.encode(this.ecPublicKey);
      publicKeyElem.appendChild(DOMUtils.getOwnerDocument(publicKeyElem).createTextNode(encoded));

      ecKeyValueElem.appendChild(publicKeyElem);
      parent.appendChild(ecKeyValueElem);
    }

    PublicKey unmarshalKeyValue(Element kvtElem)
      throws MarshalException
    {
      if (this.eckf == null) {
        try {
          this.eckf = KeyFactory.getInstance("EC");
        } catch (NoSuchAlgorithmException e) {
          throw new RuntimeException("unable to create EC KeyFactory: " + e.getMessage());
        }
      }
      try
      {
        AccessController.doPrivileged(new PrivilegedExceptionAction()
        {
          public Void run()
            throws ClassNotFoundException, NoSuchMethodException
          {
            DOMKeyValue.EC.this.getMethods();
            return null;
          }
        });
      }
      catch (PrivilegedActionException pae) {
        throw new MarshalException("ECKeyValue not supported", pae.getException());
      }

      ECParameterSpec ecParams = null;
      Element curElem = DOMUtils.getFirstChildElement(kvtElem);
      if (Utils.extractLocalName(curElem.getTagName()).equals("ECParameters")) {
        throw new UnsupportedOperationException("ECParameters not supported");
      }
      if (Utils.extractLocalName(curElem.getTagName()).equals("NamedCurve")) {
        String uri = DOMUtils.getAttributeValue(curElem, "URI");

        if (uri.startsWith("urn:oid:")) {
          String oid = uri.substring(8);
          try {
            Object[] args = { oid };
            ecParams = (ECParameterSpec)this.getECParameterSpec.invoke(null, args);
          }
          catch (IllegalAccessException iae) {
            throw new MarshalException(iae);
          } catch (InvocationTargetException ite) {
            throw new MarshalException(ite);
          }
        } else {
          throw new MarshalException("Invalid NamedCurve URI");
        }
      } else {
        throw new MarshalException("Invalid ECKeyValue");
      }
      curElem = DOMUtils.getNextSiblingElement(curElem);
      ECPoint ecPoint = null;
      try {
        Object[] args = { Base64.decode(curElem), ecParams.getCurve() };

        ecPoint = (ECPoint)this.decodePoint.invoke(null, args);
      } catch (Base64DecodingException bde) {
        throw new MarshalException("Invalid EC PublicKey", bde);
      } catch (IllegalAccessException iae) {
        throw new MarshalException(iae);
      } catch (InvocationTargetException ite) {
        throw new MarshalException(ite);
      }

      ECPublicKeySpec spec = new ECPublicKeySpec(ecPoint, ecParams);
      return DOMKeyValue.generatePublicKey(this.eckf, spec);
    }
  }

  static final class GOST3410 extends DOMKeyValue
  {
    private DOMCryptoBinary gostPublicKey;
    private KeyFactory gostkf;

    GOST3410(PublicKey key)
      throws KeyException
    {
      super(key);
      this.gostPublicKey = new DOMCryptoBinary(new BigInteger(key.getEncoded()));
    }

    GOST3410(Element elem) throws MarshalException {
      super(elem);
    }

    void marshalPublicKey(Node parent, Document doc, String dsPrefix, DOMCryptoContext context)
      throws MarshalException
    {
      Element gostElem = DOMUtils.createElement(doc, "GOSTKeyValue", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element pkElem = DOMUtils.createElement(doc, "PublicKey", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      this.gostPublicKey.marshal(pkElem, dsPrefix, context);
      gostElem.appendChild(pkElem);
      parent.appendChild(gostElem);
    }

    PublicKey unmarshalKeyValue(Element kvtElem)
      throws MarshalException
    {
      if (this.gostkf == null) {
        try {
          // TODO брать алгоритм открытого ключа из настроек
          this.gostkf = KeyFactory.getInstance("ECGOST3410");
        } catch (NoSuchAlgorithmException e) {
          throw new RuntimeException("unable to create GOST KeyFactory: " + e.getMessage());
        }

      }

      Element gostElem = DOMUtils.getFirstChildElement(kvtElem);
      this.gostPublicKey = new DOMCryptoBinary(gostElem.getFirstChild());
      PublicKey pubKey = null;
      try {
        byte[] bKey = this.gostPublicKey.getBigNum().toByteArray();
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bKey);
        pubKey = gostkf.generatePublic(keySpec);
      } catch (InvalidKeySpecException e) {
        throw new RuntimeException("unable to create GOST PublicKey: " + e.getMessage());
      }

      return pubKey;
    }
  }
  
  static final class DSA extends DOMKeyValue
  {
    private DOMCryptoBinary p;
    private DOMCryptoBinary q;
    private DOMCryptoBinary g;
    private DOMCryptoBinary y;
    private DOMCryptoBinary j;
    private KeyFactory dsakf;

    DSA(PublicKey key)
      throws KeyException
    {
      super(key);
      DSAPublicKey dkey = (DSAPublicKey)key;
      DSAParams params = dkey.getParams();
      this.p = new DOMCryptoBinary(params.getP());
      this.q = new DOMCryptoBinary(params.getQ());
      this.g = new DOMCryptoBinary(params.getG());
      this.y = new DOMCryptoBinary(dkey.getY());
    }

    DSA(Element elem) throws MarshalException {
      super(elem);
    }

    void marshalPublicKey(Node parent, Document doc, String dsPrefix, DOMCryptoContext context)
      throws MarshalException
    {
      Element dsaElem = DOMUtils.createElement(doc, "DSAKeyValue", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element pElem = DOMUtils.createElement(doc, "P", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element qElem = DOMUtils.createElement(doc, "Q", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element gElem = DOMUtils.createElement(doc, "G", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element yElem = DOMUtils.createElement(doc, "Y", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      this.p.marshal(pElem, dsPrefix, context);
      this.q.marshal(qElem, dsPrefix, context);
      this.g.marshal(gElem, dsPrefix, context);
      this.y.marshal(yElem, dsPrefix, context);
      dsaElem.appendChild(pElem);
      dsaElem.appendChild(qElem);
      dsaElem.appendChild(gElem);
      dsaElem.appendChild(yElem);
      parent.appendChild(dsaElem);
    }

    PublicKey unmarshalKeyValue(Element kvtElem)
      throws MarshalException
    {
      if (this.dsakf == null) {
        try {
          this.dsakf = KeyFactory.getInstance("DSA");
        } catch (NoSuchAlgorithmException e) {
          throw new RuntimeException("unable to create DSA KeyFactory: " + e.getMessage());
        }
      }

      Element curElem = DOMUtils.getFirstChildElement(kvtElem);

      if (Utils.extractLocalName( curElem.getTagName()).equals("P")) {
        this.p = new DOMCryptoBinary(curElem.getFirstChild());
        curElem = DOMUtils.getNextSiblingElement(curElem);
        this.q = new DOMCryptoBinary(curElem.getFirstChild());
        curElem = DOMUtils.getNextSiblingElement(curElem);
      }
      if (Utils.extractLocalName(curElem.getTagName()).equals("G")) {
        this.g = new DOMCryptoBinary(curElem.getFirstChild());
        curElem = DOMUtils.getNextSiblingElement(curElem);
      }
      this.y = new DOMCryptoBinary(curElem.getFirstChild());
      curElem = DOMUtils.getNextSiblingElement(curElem);
      if ((curElem != null) && (Utils.extractLocalName(curElem.getTagName()).equals("J"))) {
        this.j = new DOMCryptoBinary(curElem.getFirstChild());
        curElem = DOMUtils.getNextSiblingElement(curElem);
      }

      DSAPublicKeySpec spec = new DSAPublicKeySpec(this.y.getBigNum(), this.p.getBigNum(), this.q.getBigNum(), this.g.getBigNum());

      return DOMKeyValue.generatePublicKey(this.dsakf, spec);
    }
  }

  static final class RSA extends DOMKeyValue
  {
    private DOMCryptoBinary modulus;
    private DOMCryptoBinary exponent;
    private KeyFactory rsakf;

    RSA(PublicKey key)
      throws KeyException
    {
      super(key);
      RSAPublicKey rkey = (RSAPublicKey)key;
      this.exponent = new DOMCryptoBinary(rkey.getPublicExponent());
      this.modulus = new DOMCryptoBinary(rkey.getModulus());
    }

    RSA(Element elem) throws MarshalException {
      super(elem);
    }

    void marshalPublicKey(Node parent, Document doc, String dsPrefix, DOMCryptoContext context) throws MarshalException
    {
      Element rsaElem = DOMUtils.createElement(doc, "RSAKeyValue", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element modulusElem = DOMUtils.createElement(doc, "Modulus", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      Element exponentElem = DOMUtils.createElement(doc, "Exponent", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      this.modulus.marshal(modulusElem, dsPrefix, context);
      this.exponent.marshal(exponentElem, dsPrefix, context);
      rsaElem.appendChild(modulusElem);
      rsaElem.appendChild(exponentElem);
      parent.appendChild(rsaElem);
    }

    PublicKey unmarshalKeyValue(Element kvtElem)
      throws MarshalException
    {
      if (this.rsakf == null) {
        try {
          this.rsakf = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
          throw new RuntimeException("unable to create RSA KeyFactory: " + e.getMessage());
        }
      }

      Element modulusElem = DOMUtils.getFirstChildElement(kvtElem);
      this.modulus = new DOMCryptoBinary(modulusElem.getFirstChild());
      Element exponentElem = DOMUtils.getNextSiblingElement(modulusElem);
      this.exponent = new DOMCryptoBinary(exponentElem.getFirstChild());
      RSAPublicKeySpec spec = new RSAPublicKeySpec(this.modulus.getBigNum(), this.exponent.getBigNum());

      return DOMKeyValue.generatePublicKey(this.rsakf, spec);
    }
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMKeyValue
 * JD-Core Version:    0.6.2
 */