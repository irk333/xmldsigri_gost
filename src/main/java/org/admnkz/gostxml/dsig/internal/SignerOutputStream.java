package org.admnkz.gostxml.dsig.internal;

import java.io.ByteArrayOutputStream;
import java.security.Signature;
import java.security.SignatureException;

public class SignerOutputStream extends ByteArrayOutputStream
{
  private final Signature sig;

  public SignerOutputStream(Signature sig)
  {
    this.sig = sig;
  }

  public void write(int arg0)
  {
    super.write(arg0);
    try {
      this.sig.update((byte)arg0);
    } catch (SignatureException e) {
      throw new RuntimeException(e);
    }
  }

  public void write(byte[] arg0, int arg1, int arg2)
  {
    super.write(arg0, arg1, arg2);
    try {
      this.sig.update(arg0, arg1, arg2);
    } catch (SignatureException e) {
      throw new RuntimeException(e);
    }
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.SignerOutputStream
 * JD-Core Version:    0.6.2
 */