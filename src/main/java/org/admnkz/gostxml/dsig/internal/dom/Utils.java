package org.admnkz.gostxml.dsig.internal.dom;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public final class Utils
{
  public static byte[] readBytesFromStream(InputStream is)
    throws IOException
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte[] buf = new byte[1024];
    while (true) {
      int read = is.read(buf);
      if (read == -1) {
        break;
      }
      baos.write(buf, 0, read);
      if (read < 1024) {
        break;
      }
    }
    return baos.toByteArray();
  }

  static Set<Node> toNodeSet(Iterator<Node> i)
  {
    Set nodeSet = new HashSet();
    while (i.hasNext()) {
      Node n = (Node)i.next();
      nodeSet.add(n);

      if (n.getNodeType() == 1) {
        NamedNodeMap nnm = n.getAttributes();
        int j = 0; for (int length = nnm.getLength(); j < length; j++) {
          nodeSet.add(nnm.item(j));
        }
      }
    }
    return nodeSet;
  }

  public static String parseIdFromSameDocumentURI(String uri)
  {
    if (uri.length() == 0) {
      return null;
    }
    String id = uri.substring(1);
    if ((id != null) && (id.startsWith("xpointer(id("))) {
      int i1 = id.indexOf('\'');
      int i2 = id.indexOf('\'', i1 + 1);
      id = id.substring(i1 + 1, i2);
    }
    return id;
  }
  
  public static String extractLocalName(String tagName) {
      if (tagName == null) {
          return null;
      }
      
      int n = tagName.indexOf(":");
      if (n >= 0) {
          return tagName.substring(n+1);
      } else {
          return tagName;
      }
  }  

  public static boolean sameDocumentURI(String uri)
  {
    return (uri != null) && ((uri.length() == 0) || (uri.charAt(0) == '#'));
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.Utils
 * JD-Core Version:    0.6.2
 */