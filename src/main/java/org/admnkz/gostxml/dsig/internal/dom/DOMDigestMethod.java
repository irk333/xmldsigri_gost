package org.admnkz.gostxml.dsig.internal.dom;

import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.spec.DigestMethodParameterSpec;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class DOMDigestMethod extends DOMStructure
  implements DigestMethod
{
  static final String SHA384 = "http://www.w3.org/2001/04/xmldsig-more#sha384";
  private DigestMethodParameterSpec params;

  DOMDigestMethod(AlgorithmParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if ((params != null) && (!(params instanceof DigestMethodParameterSpec))) {
      throw new InvalidAlgorithmParameterException("params must be of type DigestMethodParameterSpec");
    }

    checkParams((DigestMethodParameterSpec)params);
    this.params = ((DigestMethodParameterSpec)params);
  }

  DOMDigestMethod(Element dmElem)
    throws MarshalException
  {
    Element paramsElem = DOMUtils.getFirstChildElement(dmElem);
    if (paramsElem != null)
      this.params = unmarshalParams(paramsElem);
    try
    {
      checkParams(this.params);
    } catch (InvalidAlgorithmParameterException iape) {
      throw new MarshalException(iape);
    }
  }

  static DigestMethod unmarshal(Element dmElem) throws MarshalException {
    String alg = DOMUtils.getAttributeValue(dmElem, "Algorithm");
    if (alg.equals("http://www.w3.org/2000/09/xmldsig#sha1"))
      return new SHA1(dmElem);
    if (alg.equals("http://www.w3.org/2001/04/xmlenc#sha256"))
      return new SHA256(dmElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#sha384"))
      return new SHA384(dmElem);
    if (alg.equals("http://www.w3.org/2001/04/xmlenc#sha512"))
      return new SHA512(dmElem);
    if (alg.equals("http://www.w3.org/2001/04/xmldsig-more#gostr3411"))
      return new GOST3411(dmElem);
    if (alg.equals("urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr3411")) {
      return new GOST3411URN(dmElem);
    }

    throw new MarshalException("unsupported DigestMethod algorithm: " + alg);
  }

  void checkParams(DigestMethodParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if (params != null)
      throw new InvalidAlgorithmParameterException("no parameters should be specified for the " + getMessageDigestAlgorithm() + " DigestMethod algorithm");
  }

  public final AlgorithmParameterSpec getParameterSpec()
  {
    return this.params;
  }

  DigestMethodParameterSpec unmarshalParams(Element paramsElem)
    throws MarshalException
  {
    throw new MarshalException("no parameters should be specified for the " + getMessageDigestAlgorithm() + " DigestMethod algorithm");
  }

  public void marshal(Node parent, String prefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);

    Element dmElem = DOMUtils.createElement(ownerDoc, "DigestMethod", "http://www.w3.org/2000/09/xmldsig#", prefix);

    DOMUtils.setAttribute(dmElem, "Algorithm", getAlgorithm());

    if (this.params != null) {
      marshalParams(dmElem, prefix);
    }

    parent.appendChild(dmElem);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof DigestMethod)) {
      return false;
    }
    DigestMethod odm = (DigestMethod)o;

    boolean paramsEqual = this.params == null ? false : odm.getParameterSpec() == null ? true : this.params.equals(odm.getParameterSpec());

    return (getAlgorithm().equals(odm.getAlgorithm())) && (paramsEqual);
  }

  public int hashCode()
  {
    int result = 17;
    if (this.params != null) {
      result = 31 * result + this.params.hashCode();
    }
    String algorithm = getAlgorithm();
    if (algorithm != null) {
      result = 31 * result + algorithm.hashCode();
    }

    return result;
  }

  void marshalParams(Element parent, String prefix)
    throws MarshalException
  {
    throw new MarshalException("no parameters should be specified for the " + getMessageDigestAlgorithm() + " DigestMethod algorithm");
  }

  abstract String getMessageDigestAlgorithm();

  static final class GOST3411URN extends DOMDigestMethod
  {
    GOST3411URN(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    GOST3411URN(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr3411";
    }
    String getMessageDigestAlgorithm() {
      return "GOST3411";
    }
  }

  static final class GOST3411 extends DOMDigestMethod
  {
    GOST3411(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    GOST3411(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#gostr3411";
    }
    String getMessageDigestAlgorithm() {
      return "GOST3411";
    }
  }

  static final class SHA512 extends DOMDigestMethod
  {
    SHA512(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA512(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmlenc#sha512";
    }
    String getMessageDigestAlgorithm() {
      return "SHA-512";
    }
  }

  static final class SHA384 extends DOMDigestMethod
  {
    SHA384(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA384(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmldsig-more#sha384";
    }
    String getMessageDigestAlgorithm() {
      return "SHA-384";
    }
  }

  static final class SHA256 extends DOMDigestMethod
  {
    SHA256(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA256(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2001/04/xmlenc#sha256";
    }
    String getMessageDigestAlgorithm() {
      return "SHA-256";
    }
  }

  static final class SHA1 extends DOMDigestMethod
  {
    SHA1(AlgorithmParameterSpec params)
      throws InvalidAlgorithmParameterException
    {
      super(params);
    }
    SHA1(Element dmElem) throws MarshalException {
      super(dmElem);
    }
    public String getAlgorithm() {
      return "http://www.w3.org/2000/09/xmldsig#sha1";
    }
    String getMessageDigestAlgorithm() {
      return "SHA-1";
    }
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMDigestMethod
 * JD-Core Version:    0.6.2
 */