package org.admnkz.gostxml.dsig.internal.dom;

import java.security.InvalidAlgorithmParameterException;
import javax.xml.crypto.Data;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;

public final class DOMCanonicalXMLC14NMethod extends ApacheCanonicalizer
{
  public void init(TransformParameterSpec params)
    throws InvalidAlgorithmParameterException
  {
    if (params != null)
      throw new InvalidAlgorithmParameterException("no parameters should be specified for Canonical XML C14N algorithm");
  }

  public Data transform(Data data, XMLCryptoContext xc)
    throws TransformException
  {
    if ((data instanceof DOMSubTreeData)) {
      DOMSubTreeData subTree = (DOMSubTreeData)data;
      if (subTree.excludeComments()) {
        try {
          this.apacheCanonicalizer = Canonicalizer.getInstance("http://www.w3.org/TR/2001/REC-xml-c14n-20010315");
        }
        catch (InvalidCanonicalizerException ice) {
          throw new TransformException("Couldn't find Canonicalizer for: http://www.w3.org/TR/2001/REC-xml-c14n-20010315: " + ice.getMessage(), ice);
        }

      }

    }

    return canonicalize(data, xc);
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMCanonicalXMLC14NMethod
 * JD-Core Version:    0.6.2
 */