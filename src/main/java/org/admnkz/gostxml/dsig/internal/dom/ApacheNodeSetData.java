package org.admnkz.gostxml.dsig.internal.dom;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.xml.crypto.NodeSetData;
import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Node;

public class ApacheNodeSetData
  implements ApacheData, NodeSetData
{
  private XMLSignatureInput xi;

  public ApacheNodeSetData(XMLSignatureInput xi)
  {
    this.xi = xi;
  }

  public Iterator iterator()
  {
    if (this.xi.getNodeFilters() != null) {
      return Collections.unmodifiableSet(getNodeSet(this.xi.getNodeFilters())).iterator();
    }
    try
    {
      return Collections.unmodifiableSet(this.xi.getNodeSet()).iterator();
    }
    catch (Exception e) {
      throw new RuntimeException("unrecoverable error retrieving nodeset", e);
    }
  }

  public XMLSignatureInput getXMLSignatureInput()
  {
    return this.xi;
  }

  private Set<Node> getNodeSet(List<NodeFilter> nodeFilters) {
    if (this.xi.isNeedsToBeExpanded()) {
      XMLUtils.circumventBug2650(XMLUtils.getOwnerDocument(this.xi.getSubNode()));
    }

    Set<Node> inputSet = new LinkedHashSet();
    XMLUtils.getSet(this.xi.getSubNode(), inputSet, null, !this.xi.isExcludeComments());

    Set nodeSet = new LinkedHashSet();
    for (Node currentNode : inputSet) {
      Iterator it = nodeFilters.iterator();
      boolean skipNode = false;
      while ((it.hasNext()) && (!skipNode)) {
        NodeFilter nf = (NodeFilter)it.next();
        if (nf.isNodeInclude(currentNode) != 1) {
          skipNode = true;
        }
      }
      if (!skipNode) {
        nodeSet.add(currentNode);
      }
    }
    return nodeSet;
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.ApacheNodeSetData
 * JD-Core Version:    0.6.2
 */