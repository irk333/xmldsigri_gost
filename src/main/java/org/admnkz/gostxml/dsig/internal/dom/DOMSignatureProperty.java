package org.admnkz.gostxml.dsig.internal.dom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.SignatureProperty;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class DOMSignatureProperty extends DOMStructure
  implements SignatureProperty
{
  private final String id;
  private final String target;
  private final List<XMLStructure> content;

  public DOMSignatureProperty(List<? extends XMLStructure> content, String target, String id)
  {
    if (target == null)
      throw new NullPointerException("target cannot be null");
    if (content == null)
      throw new NullPointerException("content cannot be null");
    if (content.isEmpty()) {
      throw new IllegalArgumentException("content cannot be empty");
    }
    this.content = Collections.unmodifiableList(new ArrayList(content));

    int i = 0; for (int size = this.content.size(); i < size; i++) {
      if (!(this.content.get(i) instanceof XMLStructure)) {
        throw new ClassCastException("content[" + i + "] is not a valid type");
      }

    }

    this.target = target;
    this.id = id;
  }

  public DOMSignatureProperty(Element propElem, XMLCryptoContext context)
    throws MarshalException
  {
    this.target = DOMUtils.getAttributeValue(propElem, "Target");
    if (this.target == null) {
      throw new MarshalException("target cannot be null");
    }
    Attr attr = propElem.getAttributeNodeNS(null, "Id");
    if (attr != null) {
      this.id = attr.getValue();
      propElem.setIdAttributeNode(attr, true);
    } else {
      this.id = null;
    }

    NodeList nodes = propElem.getChildNodes();
    int length = nodes.getLength();
    List content = new ArrayList(length);
    for (int i = 0; i < length; i++) {
      content.add(new javax.xml.crypto.dom.DOMStructure(nodes.item(i)));
    }
    if (content.isEmpty()) {
      throw new MarshalException("content cannot be empty");
    }
    this.content = Collections.unmodifiableList(content);
  }

  public List getContent()
  {
    return this.content;
  }

  public String getId() {
    return this.id;
  }

  public String getTarget() {
    return this.target;
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);
    Element propElem = DOMUtils.createElement(ownerDoc, "SignatureProperty", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMUtils.setAttributeID(propElem, "Id", this.id);
    DOMUtils.setAttribute(propElem, "Target", this.target);

    for (XMLStructure property : this.content) {
      DOMUtils.appendChild(propElem, ((javax.xml.crypto.dom.DOMStructure)property).getNode());
    }

    parent.appendChild(propElem);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof SignatureProperty)) {
      return false;
    }
    SignatureProperty osp = (SignatureProperty)o;

    boolean idsEqual = this.id == null ? false : osp.getId() == null ? true : this.id.equals(osp.getId());

    List ospContent = osp.getContent();
    return (equalsContent(ospContent)) && (this.target.equals(osp.getTarget())) && (idsEqual);
  }

  private boolean equalsContent(List<XMLStructure> otherContent)
  {
    int osize = otherContent.size();
    if (this.content.size() != osize) {
      return false;
    }
    for (int i = 0; i < osize; i++) {
      XMLStructure oxs = (XMLStructure)otherContent.get(i);
      XMLStructure xs = (XMLStructure)this.content.get(i);
      if ((oxs instanceof javax.xml.crypto.dom.DOMStructure)) {
        if (!(xs instanceof javax.xml.crypto.dom.DOMStructure)) {
          return false;
        }
        Node onode = ((javax.xml.crypto.dom.DOMStructure)oxs).getNode();
        Node node = ((javax.xml.crypto.dom.DOMStructure)xs).getNode();
        if (!DOMUtils.nodesEqual(node, onode)) {
          return false;
        }
      }
      else if (!xs.equals(oxs)) {
        return false;
      }

    }

    return true;
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMSignatureProperty
 * JD-Core Version:    0.6.2
 */