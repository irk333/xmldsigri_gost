package org.admnkz.gostxml.dsig.internal.dom;

import java.security.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.XMLObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class DOMXMLObject extends DOMStructure
  implements XMLObject
{
  private final String id;
  private final String mimeType;
  private final String encoding;
  private final List<XMLStructure> content;
  private Element objectElem;

  public DOMXMLObject(List<? extends XMLStructure> content, String id, String mimeType, String encoding)
  {
    if ((content == null) || (content.isEmpty())) {
      this.content = Collections.emptyList();
    } else {
      this.content = Collections.unmodifiableList(new ArrayList(content));

      int i = 0; for (int size = this.content.size(); i < size; i++) {
        if (!(this.content.get(i) instanceof XMLStructure)) {
          throw new ClassCastException("content[" + i + "] is not a valid type");
        }
      }
    }

    this.id = id;
    this.mimeType = mimeType;
    this.encoding = encoding;
  }

  public DOMXMLObject(Element objElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    this.encoding = DOMUtils.getAttributeValue(objElem, "Encoding");

    Attr attr = objElem.getAttributeNodeNS(null, "Id");
    if (attr != null) {
      this.id = attr.getValue();
      objElem.setIdAttributeNode(attr, true);
    } else {
      this.id = null;
    }
    this.mimeType = DOMUtils.getAttributeValue(objElem, "MimeType");

    NodeList nodes = objElem.getChildNodes();
    int length = nodes.getLength();
    List content = new ArrayList(length);
    for (int i = 0; i < length; i++) {
      Node child = nodes.item(i);
      if (child.getNodeType() == 1) {
        Element childElem = (Element)child;
        String tag = Utils.extractLocalName(childElem.getTagName());
        if (tag.equals("Manifest")) {
          content.add(new DOMManifest(childElem, context, provider));
          continue;
        }if (tag.equals("SignatureProperties")) {
          content.add(new DOMSignatureProperties(childElem, context));
          continue;
        }if (tag.equals("X509Data")) {
          content.add(new DOMX509Data(childElem));
          continue;
        }
      }

      content.add(new javax.xml.crypto.dom.DOMStructure(child));
    }
    if (content.isEmpty())
      this.content = Collections.emptyList();
    else {
      this.content = Collections.unmodifiableList(content);
    }
    this.objectElem = objElem;
  }

  public List getContent() {
    return this.content;
  }

  public String getId() {
    return this.id;
  }

  public String getMimeType() {
    return this.mimeType;
  }

  public String getEncoding() {
    return this.encoding;
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context) throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);

    Element objElem = this.objectElem != null ? this.objectElem : null;
    if (objElem == null) {
      objElem = DOMUtils.createElement(ownerDoc, "Object", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

      DOMUtils.setAttributeID(objElem, "Id", this.id);
      DOMUtils.setAttribute(objElem, "MimeType", this.mimeType);
      DOMUtils.setAttribute(objElem, "Encoding", this.encoding);

      for (XMLStructure object : this.content) {
        if ((object instanceof DOMStructure)) {
          ((DOMStructure)object).marshal(objElem, dsPrefix, context);
        } else {
          javax.xml.crypto.dom.DOMStructure domObject = (javax.xml.crypto.dom.DOMStructure)object;

          DOMUtils.appendChild(objElem, domObject.getNode());
        }
      }
    }

    parent.appendChild(objElem);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof XMLObject)) {
      return false;
    }
    XMLObject oxo = (XMLObject)o;

    boolean idsEqual = this.id == null ? false : oxo.getId() == null ? true : this.id.equals(oxo.getId());

    boolean encodingsEqual = this.encoding == null ? false : oxo.getEncoding() == null ? true : this.encoding.equals(oxo.getEncoding());

    boolean mimeTypesEqual = this.mimeType == null ? false : oxo.getMimeType() == null ? true : this.mimeType.equals(oxo.getMimeType());

    List oxoContent = oxo.getContent();
    return (idsEqual) && (encodingsEqual) && (mimeTypesEqual) && (equalsContent(oxoContent));
  }

  private boolean equalsContent(List<XMLStructure> otherContent)
  {
    if (this.content.size() != otherContent.size()) {
      return false;
    }
    int i = 0; for (int osize = otherContent.size(); i < osize; i++) {
      XMLStructure oxs = (XMLStructure)otherContent.get(i);
      XMLStructure xs = (XMLStructure)this.content.get(i);
      if ((oxs instanceof javax.xml.crypto.dom.DOMStructure)) {
        if (!(xs instanceof javax.xml.crypto.dom.DOMStructure)) {
          return false;
        }
        Node onode = ((javax.xml.crypto.dom.DOMStructure)oxs).getNode();
        Node node = ((javax.xml.crypto.dom.DOMStructure)xs).getNode();
        if (!DOMUtils.nodesEqual(node, onode)) {
          return false;
        }
      }
      else if (!xs.equals(oxs)) {
        return false;
      }

    }

    return true;
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMXMLObject
 * JD-Core Version:    0.6.2
 */