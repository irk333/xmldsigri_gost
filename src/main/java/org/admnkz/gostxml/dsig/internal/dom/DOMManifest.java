package org.admnkz.gostxml.dsig.internal.dom;

import java.security.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.Manifest;
import javax.xml.crypto.dsig.Reference;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public final class DOMManifest extends DOMStructure
  implements Manifest
{
  private final List<Reference> references;
  private final String id;

  public DOMManifest(List<? extends Reference> references, String id)
  {
    if (references == null) {
      throw new NullPointerException("references cannot be null");
    }
    this.references = Collections.unmodifiableList(new ArrayList(references));

    if (this.references.isEmpty()) {
      throw new IllegalArgumentException("list of references must contain at least one entry");
    }

    int i = 0; for (int size = this.references.size(); i < size; i++) {
      if (!(this.references.get(i) instanceof Reference)) {
        throw new ClassCastException("references[" + i + "] is not a valid type");
      }
    }

    this.id = id;
  }

  public DOMManifest(Element manElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    Attr attr = manElem.getAttributeNodeNS(null, "Id");
    if (attr != null) {
      this.id = attr.getValue();
      manElem.setIdAttributeNode(attr, true);
    } else {
      this.id = null;
    }

    Boolean secureValidation = (Boolean)context.getProperty("org.apache.jcp.xml.dsig.secureValidation");

    boolean secVal = false;
    if ((secureValidation != null) && (secureValidation.booleanValue())) {
      secVal = true;
    }

    Element refElem = DOMUtils.getFirstChildElement(manElem);
    List refs = new ArrayList();

    int refCount = 0;
    while (refElem != null) {
      refs.add(new DOMReference(refElem, context, provider));
      refElem = DOMUtils.getNextSiblingElement(refElem);

      refCount++;
      if ((secVal) && (refCount > 30)) {
        String error = "A maxiumum of 30 references per Manifest are allowed with secure validation";

        throw new MarshalException(error);
      }
    }
    this.references = Collections.unmodifiableList(refs);
  }

  public String getId() {
    return this.id;
  }

  public List getReferences() {
    return this.references;
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    Document ownerDoc = DOMUtils.getOwnerDocument(parent);
    Element manElem = DOMUtils.createElement(ownerDoc, "Manifest", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMUtils.setAttributeID(manElem, "Id", this.id);

    for (Reference ref : this.references) {
      ((DOMReference)ref).marshal(manElem, dsPrefix, context);
    }
    parent.appendChild(manElem);
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof Manifest)) {
      return false;
    }
    Manifest oman = (Manifest)o;

    boolean idsEqual = this.id == null ? false : oman.getId() == null ? true : this.id.equals(oman.getId());

    return (idsEqual) && (this.references.equals(oman.getReferences()));
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMManifest
 * JD-Core Version:    0.6.2
 */