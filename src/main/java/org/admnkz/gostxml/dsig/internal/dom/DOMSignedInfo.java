package org.admnkz.gostxml.dsig.internal.dom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.XMLSignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.UnsyncBufferedOutputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public final class DOMSignedInfo extends DOMStructure
  implements SignedInfo
{
  public static final int MAXIMUM_REFERENCE_COUNT = 30;
  private static Log log = LogFactory.getLog(DOMSignedInfo.class);
  private static final String ALGO_ID_SIGNATURE_NOT_RECOMMENDED_RSA_MD5 = "http://www.w3.org/2001/04/xmldsig-more#rsa-md5";
  private static final String ALGO_ID_MAC_HMAC_NOT_RECOMMENDED_MD5 = "http://www.w3.org/2001/04/xmldsig-more#hmac-md5";
  private List<Reference> references;
  private CanonicalizationMethod canonicalizationMethod;
  private SignatureMethod signatureMethod;
  private String id;
  private Document ownerDoc;
  private Element localSiElem;
  private InputStream canonData;

  public DOMSignedInfo(CanonicalizationMethod cm, SignatureMethod sm, List<? extends Reference> references)
  {
    if ((cm == null) || (sm == null) || (references == null)) {
      throw new NullPointerException();
    }
    this.canonicalizationMethod = cm;
    this.signatureMethod = sm;
    this.references = Collections.unmodifiableList(new ArrayList(references));

    if (this.references.isEmpty()) {
      throw new IllegalArgumentException("list of references must contain at least one entry");
    }

    int i = 0; for (int size = this.references.size(); i < size; i++) {
      Object obj = this.references.get(i);
      if (!(obj instanceof Reference))
        throw new ClassCastException("list of references contains an illegal type");
    }
  }

  public DOMSignedInfo(CanonicalizationMethod cm, SignatureMethod sm, List<? extends Reference> references, String id)
  {
    this(cm, sm, references);
    this.id = id;
  }

  public DOMSignedInfo(Element siElem, XMLCryptoContext context, Provider provider)
    throws MarshalException
  {
    this.localSiElem = siElem;
    this.ownerDoc = siElem.getOwnerDocument();

    this.id = DOMUtils.getAttributeValue(siElem, "Id");

    Element cmElem = DOMUtils.getFirstChildElement(siElem);
    this.canonicalizationMethod = new DOMCanonicalizationMethod(cmElem, context, provider);

    Element smElem = DOMUtils.getNextSiblingElement(cmElem);
    this.signatureMethod = DOMSignatureMethod.unmarshal(smElem);

    Boolean secureValidation = (Boolean)context.getProperty("org.apache.jcp.xml.dsig.secureValidation");

    boolean secVal = false;
    if ((secureValidation != null) && (secureValidation.booleanValue())) {
      secVal = true;
    }

    if ((secVal) && (("http://www.w3.org/2001/04/xmldsig-more#hmac-md5".equals(this.signatureMethod)) || ("http://www.w3.org/2001/04/xmldsig-more#rsa-md5".equals(this.signatureMethod))))
    {
      throw new MarshalException(new StringBuilder().append("It is forbidden to use algorithm ").append(this.signatureMethod).append(" when secure validation is enabled").toString());
    }

    ArrayList refList = new ArrayList(5);
    Element refElem = DOMUtils.getNextSiblingElement(smElem);

    int refCount = 0;
    while (refElem != null) {
      refList.add(new DOMReference(refElem, context, provider));
      refElem = DOMUtils.getNextSiblingElement(refElem);

      refCount++;
      if ((secVal) && (refCount > 30)) {
        String error = "A maxiumum of 30 references per Manifest are allowed with secure validation";

        throw new MarshalException(error);
      }
    }
    this.references = Collections.unmodifiableList(refList);
  }

  public CanonicalizationMethod getCanonicalizationMethod() {
    return this.canonicalizationMethod;
  }

  public SignatureMethod getSignatureMethod() {
    return this.signatureMethod;
  }

  public String getId() {
    return this.id;
  }

  public List getReferences() {
    return this.references;
  }

  public InputStream getCanonicalizedData() {
    return this.canonData;
  }

  public void canonicalize(XMLCryptoContext context, ByteArrayOutputStream bos) throws XMLSignatureException
  {
    if (context == null) {
      throw new NullPointerException("context cannot be null");
    }

    OutputStream os = new UnsyncBufferedOutputStream(bos);
    try {
      os.close();
    } catch (IOException e) {
      if (log.isDebugEnabled()) {
        log.debug(e);
      }

    }

    DOMSubTreeData subTree = new DOMSubTreeData(this.localSiElem, true);
    try
    {
      ((DOMCanonicalizationMethod)this.canonicalizationMethod).canonicalize(subTree, context, bos);
    }
    catch (TransformException te) {
      throw new XMLSignatureException(te);
    }

    byte[] signedInfoBytes = bos.toByteArray();

    if (log.isDebugEnabled()) {
      log.debug("Canonicalized SignedInfo:");
      StringBuilder sb = new StringBuilder(signedInfoBytes.length);
      for (int i = 0; i < signedInfoBytes.length; i++) {
        sb.append((char)signedInfoBytes[i]);
      }
      log.debug(sb.toString());
      log.debug(new StringBuilder().append("Data to be signed/verified:").append(Base64.encode(signedInfoBytes)).toString());
    }

    this.canonData = new ByteArrayInputStream(signedInfoBytes);
  }

  public void marshal(Node parent, String dsPrefix, DOMCryptoContext context)
    throws MarshalException
  {
    this.ownerDoc = DOMUtils.getOwnerDocument(parent);
    Element siElem = DOMUtils.createElement(this.ownerDoc, "SignedInfo", "http://www.w3.org/2000/09/xmldsig#", dsPrefix);

    DOMCanonicalizationMethod dcm = (DOMCanonicalizationMethod)this.canonicalizationMethod;

    dcm.marshal(siElem, dsPrefix, context);

    ((DOMStructure)this.signatureMethod).marshal(siElem, dsPrefix, context);

    for (Reference reference : this.references) {
      ((DOMReference)reference).marshal(siElem, dsPrefix, context);
    }

    DOMUtils.setAttributeID(siElem, "Id", this.id);

    parent.appendChild(siElem);
    this.localSiElem = siElem;
  }

  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }

    if (!(o instanceof SignedInfo)) {
      return false;
    }
    SignedInfo osi = (SignedInfo)o;

    boolean idEqual = this.id == null ? false : osi.getId() == null ? true : this.id.equals(osi.getId());

    return (this.canonicalizationMethod.equals(osi.getCanonicalizationMethod())) && (this.signatureMethod.equals(osi.getSignatureMethod())) && (this.references.equals(osi.getReferences())) && (idEqual);
  }
}

/* Location:           /home/irina/install/cryptopro/test_jcp_wss4j1_6/XMLDSigRI.jar
 * Qualified Name:     ru.CryptoPro.JCPxml.dsig.internal.dom.DOMSignedInfo
 * JD-Core Version:    0.6.2
 */